import {message, Modal, notification} from "antd";
import {$app, $stage, isUndefined, toUndefined} from "../index";
import Cookies from 'js-cookie';

export default class API {


    static baseUrl = "https://git.venity.site/";
    static newBranch(repo,branch,cb){

        API.method("repository/branches/create", {
            repository: repo,
            token: $stage.app.token,
            name: branch
        }, (f) => {
            if (!!f.error){
                cb(false, f.error)
            } else {
                cb(true, f)
            }
        })
    }
    static setDefBranch(repo,branch,cb){

        API.method("repository/branches/default/set", {
            repository: repo,
            token: $stage.app.token,
            branch: branch
        }, (f) => {
            if (!!f.error){
                cb(false, f.error)
            } else {
                cb(true, f)
            }
        })
    }
    static removeBranch(repo,branch,cb){

        API.method("repository/branches/remove", {
            repository: repo,
            token: $stage.app.token,
            name: branch
        }, (f) => {
            if (!!f.error){
                cb(false, f.error)
            } else {
                cb(true, f)
            }
        })
    }
    static getBranches(repo, cb){
        API.method("repository/branches", {
            repository: repo,
            token: $stage.app.token
        }, (f) => {
            if (!!f.error){
                cb(false, f.error)
            } else {
                cb(true, f)
            }
        })
    }
    static getDefaultBranch(repo, cb){
        API.method("repository/branches/default", {
            repository: repo,
            token: $stage.app.token
        }, (f) => {
            if (!!f.error){
                cb(false, f.error)
            } else {
                cb(true, f)
            }
        })
    }

    static time() {
        return parseInt((new Date().getTime() / 1000) + "")
    }

    static getFileContent(repo, path, cb){

        const final = {};
        const params = {
            repository: repo,
            path: path,
            token: $stage.app.token
        };
        Object.keys(params).forEach(p => {
            const par = params[p];
            if (!isUndefined(par)){
                final[p] = par;
            }
        });
        const url = API.baseUrl + "api/repository/raw/file?" + API.encodeParams(final);

        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.onerror = function (e) {
            cb({
                error: {
                    code: -1,
                    message: e.message,
                    exception: e
                }
            }, false)
        };
        xhr.onload = function () {
            cb(xhr.response, true, xhr);
        };
        xhr.send();
    }

    static loadRepoByPath(path, cb) {

        API.method("repository/search", {
            path: path,
            token: $stage.app.token
        }, (res) => {
            if (!!res.error) {
                cb(false, res.error)
            } else {

                if (!$stage.app.repos)
                    $stage.app.repos = {};
                $stage.app.repos[res.id] = res;
                $stage.app.reposPaths[res.path] = res;
                cb(true, res);
            }
        })
    }
    static getRepoByPath(path, cb) {

        API.method("repository/search", {
            path: path,
            token: $stage.app.token
        }, (res) => {
            if (!!res.error) {
                cb(false, res.error)
            } else {
                cb(true, res);
            }
        })
    }
    static getCommits(path, count, offset, cb) {

        API.method("repository/commits", {
            repository: path,
            token: $stage.app.token,
            branch: 'refs/heads/master',
            count: count,
            offset: offset
        }, (commits) => {

            if (!!commits.error) {
                cb(false, commits.error)
            } else {
                cb(true, commits);
            }
        })
    }
    static getCommitsCount(path, cb) {

        API.method("repository/commits/count", {
            repository: path,
            token: $stage.app.token,
            branch: 'refs/heads/master'
        }, (commits) => {

            if (!!commits.error) {
                cb(false, commits.error)
            } else {
                cb(true, commits);
            }
        })
    }

    static f = {
        "js": "javascript",
        "gradle": "groovy",
        "bat": "batch",
        "md": "markdown",
        "gitignore": "git"
    };

    static loadRepoById(id, cb) {
        API.method("repository/search/id", {
            id: id
        }, (res) => {
            if (!!res.error) {
                cb(false, res.error)
            } else {

                if (!$stage.app.repos)
                    $stage.app.repos = {};
                $stage.app.repos[res.id] = res;
                $stage.app.reposPaths[res.path] = res;

            }
        })
    }

    static getDiffs(repo, newCommit, cb) {
        API.method("repository/diff", {
            repository: repo,
            newCommit: newCommit
        }, (d) => {
            if (!!d.error) {
                cb(false, d.error)
            } else {
                cb(true, d);
            }
        })
    }

    static getTree(repoPath, path, cb) {
        const args = {
            repository: repoPath,
            path: path
        };
        if (!!$stage.app.token) {
            args['token'] = $stage.app.token;
        }

        API.method("repository/raw/tree", args, cb)
    }
    static undefinedStringToEmptyString(str){
        if (str === undefined){
            return null
        }
        if (str === 'undefined'){
            return null
        }
        return str;
    }
    static updateRepos(cb) {
        if (!!$stage.app.account) {
            const a = $stage.app.account.repositories;
            if (Object.keys(a).length > 0) {
                let ids = "";
                Object.keys(a).forEach(k => {
                    ids += a[k] + ",";
                });
                ids = ids.substr(0, ids.length - 1);

                API.method("repository/search/ids", {
                    ids: ids,
                    token: $stage.app.token
                }, (r) => {

                    if (!$stage.app.repos)
                        $stage.app.repos = {};
                    for (const rOd in r) {
                        const repo = r[rOd];
                        $stage.app.repos[repo.id] = repo;
                        $stage.app.reposPaths[repo.path] = repo;
                    }
                    if (!!cb)
                        cb(true);
                })
            }
        }
    }

    static getIssuesCount(path, cb) {
        API.method("repository/issues/count", {
            repository: path,
            token: $stage.app.token
        }, (issues) => {

            if (!!issues.error) {
                cb(false, issues.error)
            } else {
                cb(true, issues);
            }
        })
    }

    static isProfileUpdating = false;

    static updateMe(cb) {
        if (!API.isProfileUpdating) {
            if ($stage.app.token) {
                API.isProfileUpdating = true;
                const m = message.loading("Sync profile...", 0);
                API.method("user.get/token", {
                    token: $stage.app.token
                }, (d) => {
                    API.isProfileUpdating = false;
                    m();
                    if (!!d.error) {
                        if (!!d.error.exception) {
                            notification.warn({
                                message: "Failed update profile (" + d.error.code + ")",
                                description: d.error.message,
                                duration: 5
                            });
                            cb(false);
                            return;
                        }
                        cb(false);
                        Modal.error({
                            title: "Failed login",
                            content: "Automatic login token expired"
                        });
                        $stage.ui.page = "main";
                        $stage.app.token = undefined;
                    } else {
                        $stage.app.account = d;
                        cb(true);
                    }
                });
            }
        }

    }

    static getParts(url) {
        return url.split("/");
    }

    static getOrigin() {
        return "http://" + API.getParts(window.location.href)[2];
    }

    static setPage(url) {
        //alert("Loc: " + location.href + "\nTo: " + url + "\nOrigin: " + API.getOrigin() + "\nReplaced: " + this.getOrigin() +"/"+ url);
        if ((this.getOrigin() +"/"+ url) !== window.location.href ) {
            const fff = document.getElementById("content");
            const ffff = document.getElementById("subContent");
            if (!!ffff)
                ffff.className = "animated fadeOut animatedFast";
            else if (!!fff)
                fff.className = "animated fadeOut animatedFast";
            setTimeout(() => {

                const parts = url.split("/");
                window.history.pushState(url, "VGit: " + url, '/' + url);
                if (parts.length < 0 || toUndefined(parts[0]) === undefined || parts[0] === '') {
                    $stage.ui.page = "main";
                } else {
                    $stage.ui.page = parts[0];
                }
                $app.forceUpdate();
            }, 200);
        }
    }

    static isDirectory(repo, path, cb) {
        API.method("repository/raw/isDirectory", {
            repository: repo,
            path: path,
            token: $stage.app.token
        }, (d) => {
            if (!!d.error) {
                cb(false, d.error);
            } else {
                cb(true, d);
            }
        })
    }

    static getLang(file) {
        const p = file.split(".");
        if (!!this.f[p[p.length - 1]]) {
            return this.f[p[p.length - 1]];
        }
        return p[p.length - 1];
    }

    static method(method, params, cb) {
        const final = {};
        Object.keys(params).forEach(p => {
            const par = params[p];
            if (!isUndefined(par)){
                final[p] = par;
            }
        });
        const url = API.baseUrl + "api/" + method.replace(".", "/") + "?" + API.encodeParams(final);
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onerror = function (e) {
            cb({
                error: {
                    code: -1,
                    message: e.message,
                    exception: e
                }
            })
        };
        xhr.onload = function () {
            cb(xhr.response, xhr);
        };
        xhr.send();
    }

    static encodeParams(params) {
        const esc = encodeURIComponent;
        return Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
    }

    static getCookie(name, def) {
        const v = Cookies.get(name);
        if (!v)
            return def;
        return v;
    }
}

export class TOOLZ {
    static setTitle(title) {
        document.title = title + " - VGit";
    }
    static isWidthAdaptive(y, n){

        const {w} = TOOLZ.getViewportSize();

        // = (w > 1000 ? '285px' : '100%');
        return (w > 1060 ? n : y);
    }

    static copy(text) {
        navigator.clipboard.writeText(text).then(function () {
            message.success("Copied!")
        }, function () {
            message.error("Copy failed!");
        });
    }

    static getViewportSize(w) {

        // Use the specified window or the current window if no argument
        w = w || window;

        // This works for all browsers except IE8 and before
        if (w.innerWidth != null) return { w: w.innerWidth, h: w.innerHeight };

        // For IE (or any browser) in Standards mode
        const d = w.document;
        if (document.compatMode === "CSS1Compat")
            return { w: d.documentElement.clientWidth,
                h: d.documentElement.clientHeight };

        // For browsers in Quirks mode
        return { w: d.body.clientWidth, h: d.body.clientHeight };

    }
}