import {isUndefined} from "../index";

export const $BASE_URL = "https://git.venity.site/"; // ТУТ //

export class VGitAPIManager {
    user = new VGitAPIUser();
    repository = new VGitAPIRepository();
    auth = new VGitAPIAuth();
    repositorySearch = new VGitAPIRepositorySearch();
    server = new VGitAPIServer();

    static encodeParams(params) {
        const esc = encodeURIComponent;
        return Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
    }

    static call(method, params, callback) {

        const final = {};
        Object.keys(params).forEach(p => {
            const par = params[p];
            if (!isUndefined(par)) {
                final[p] = par;
            }
        });
        const url = $BASE_URL + "api/" + method.replace(".", "/") + "?" + VGitAPIManager.encodeParams(final);
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onerror = function (e) {
            callback({
                error: {
                    code: -1,
                    message: e.message,
                    exception: e
                }
            })
        };
        xhr.onload = function () {
            callback(xhr.response, xhr);
        };
        xhr.send();
    }

}

export class VGitAPIModule {
    uri;

    constructor(uri) {
        this.uri = uri;
    }

    run(method, params, callback) {
        VGitAPIManager.call((!!this.uri ? (this.uri + "/") : "") + method, params, (res) => {
            callback(new VGitAPIResponse(res));
        });
    }
}

export class VGitAPIServer extends VGitAPIModule {
    constructor() {
        super(undefined);
    }

    ping(callback) {
        this.run("ping", {}, callback)
    }
}

export class VGitAPIUser extends VGitAPIModule {
    constructor() {
        super("user")

    }

    getById(id, callback) {
    }

    getByLogin(login, callback) {

    }

    getByToken(token, callback) {
        this.run("get/token", {
            token: token
        }, callback)
    }

    setStatus(status, callback) {

    }
}

export class VGitAPIRepository {
    getById(id, callback) {

    }

    getByLogin(login, callback) {

    }

    getByToken(token, callback) {

    }

    setStatus(status, callback) {

    }
}

export class VGitAPIRepositorySearch extends VGitAPIModule {
    constructor() {
        super("repository/search")

    }

    getById(id, token) {
        return null;
    }

    getByIds(ids, token, callback) {
        this.run("ids", {
            ids,
            token
        }, callback);
    }

    getByPath(path, token) {
        return null;
    }
}

export class VGitAPIRepositoryBranches {
    count(repository, token, callback) {

    }

    getDefaultBranch(repository, token, callback) {

    }

    setDefaultBranch(repository, branch, token, callback) {

    }

    hasBranch(repository, branch, token, callback) {

    }

    createBranch(repository, branch, token, callback) {

    }

    removeBranch(repository, branch, token, callback) {

    }

    getAll(repository, token, callback) {

    }
}

export class VGitAPIAuth {
    register(login, fullName, email, password, callback) {

    }

    login(login, password, callback) {

    }

    logout(token, callback) {

    }
}

export class VGitAPIResponse {
    ok;
    data;
    error;

    constructor(res) {
        this.ok = res.error === undefined;
        this.error = res.error;
        this.data = res;
    }

    asError() {
        return this.error;
    }

    asUser() {
        return this.data;
    }

    asRepo() {
        return this.data;
    }

    asRepoArray() {
        return this.data;
    }

}

