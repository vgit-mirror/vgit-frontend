export class VGitUser {
    id: number;
    registration: number;
    status: string;
    login: string;
    fullName: string;
    email: string;
    group: string;
    repositories: Array<number>;
    ban: boolean;
}

export class VGitRepository {
    id: number;
    path: string;
    name: string;
    description: string;
    owners: Array<number>;
    members: Array<number>;
    confidential: boolean;
    stars: number;
}

export class VGitDifference {

    changeType: string;
    oldPath: string;
    newPath: string;
    oldFile: string;
    newFile: string;
    oldFileMode: number;
    newFileMode: number;
}

export class VGitIssue {
    id: number;
    name: string;
    isOpen: boolean;
    labels: Array<VGitIssueTag>;
}

export class VGitIssueComment {
    id: number;
    creator: number;
    content: string;
    time: number;
}

export class VGitIssueTag {
    id: number;
    name: string;
    color: string;
}