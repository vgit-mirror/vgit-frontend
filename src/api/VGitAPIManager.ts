import {VGitRepository, VGitUser} from "./VGitPrototypes";

export class VGitAPIManager {
    user: VGitAPIUser;
    repository: VGitAPIRepository;
    auth: VGitAPIAuth;
    repositorySearch: VGitAPIRepositorySearch;
    server: VGitAPIServer;

    call(method: string, params: object, callback: (object) => void) {

    }

}

export class VGitAPIModule {
    uri: string;

    constructor(uri: string) {
        this.uri = uri;
    }

    run(method: string, params: object, callback: (response: VGitAPIResponse<any>) => void) {
    }
}

export class VGitAPIUser {
    getById(id: number, callback: (response: VGitAPIResponse<VGitUser>) => void) {

    }

    getByLogin(login: string, callback: (response: VGitAPIResponse<VGitUser>) => void) {

    }

    getByToken(token: string, callback: (response: VGitAPIResponse<VGitUser>) => void) {

    }

    setStatus(status: string, callback: (response: VGitAPIResponse<VGitUser>) => void) {

    }
}

export class VGitAPIServer extends VGitAPIModule {
    ping(callback: (response: VGitAPIResponse<string>) => void) {

    }
}

export class VGitAPIRepository extends VGitAPIModule {
    makeRepo(token: string, name: string, path: string, description: string, confidential: boolean, callback: (response: VGitAPIResponse<VGitRepository>) => void) {

    }

    removeRepo(token: string, name: string, callback: (response: VGitAPIResponse<VGitRepository>) => void) {

    }
}

export class VGitAPIRepositoryBranches extends VGitAPIModule {
    count(repository: string, token: string, callback: (response: VGitAPIResponse<number>) => void) {

    }

    getDefaultBranch(repository: string, token: string, callback: (response: VGitAPIResponse<Array<String>>) => void) {

    }

    setDefaultBranch(repository: string, branch: string, token: string, callback: (response: VGitAPIResponse<Array<String>>) => void) {

    }

    hasBranch(repository: string, branch: string, token: string, callback: (response: VGitAPIResponse<Array<String>>) => void) {

    }

    createBranch(repository: string, branch: string, token: string, callback: (response: VGitAPIResponse<Array<String>>) => void) {

    }

    removeBranch(repository: string, branch: string, token: string, callback: (response: VGitAPIResponse<Array<String>>) => void) {

    }

    getAll(repository: string, token: string, callback: (response: VGitAPIResponse<Array<string>>) => void) {

    }
}

export class VGitAPIRepositorySearch extends VGitAPIModule {
    getById(id: number, token: string, callback: (response: VGitAPIResponse<VGitRepository>) => void) {

    }

    getByIds(ids: Array<number>, token: string, callback: (response: VGitAPIResponse<VGitRepository>) => void) {

    }

    getByPath(path: string, token: string, callback: (response: VGitAPIResponse<VGitRepository>) => void) {

    }
}

export class VGitAPIAuth extends VGitAPIModule {
    register(login: string, fullName: string, email: string, password: string, callback: (response: VGitAPIResponse<string>) => void) {

    }

    login(login: string, password: string, callback: (response: VGitAPIResponse<string>) => void) {

    }

    logout(token: string) {

    }
}

export class VGitAPIError {
    message: string;
    code: number;
    exception: Error;
}

export class VGitAPIResponse<r> {

    ok: boolean;
    data: r;
    error: VGitAPIError;

    constructor(res: any) {

    }

    asError(): VGitAPIError {
        return null;
    }

    asUser(): VGitUser {
        return null;
    }

    asRepo(): VGitRepository {
        return null;
    }

    asRepoArray(): Array<VGitRepository> {
        return null;
    }
}

