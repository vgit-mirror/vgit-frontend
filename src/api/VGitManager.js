export class VGitManager {

    static VGitPageManager = new VGitPageManager();

}

export class VGitPageManager {

    pages = {};

    registerPage(page, loader) {
        this.pages[page] = loader;
    }

    isPageRegistered(page) {
        return this.pages[page] !== undefined;
    }


    getPageLoader(page) {
        if (!this.isPageRegistered(page)) {
            return null;
        }
        return this.pages[page];
    }
}