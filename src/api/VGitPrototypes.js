export class VGitUser {
    id;
    registration;
    status;
    login;
    fullName;
    email;
    group;
    repositories;
    ban;
}
export class VGitRepository {
    id;
    registration;
    status;
    login;
    fullName;
    email;
    group;
    repositories;
    ban;
}