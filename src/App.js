import React, {Component} from 'react';
import './vui/css/App.scss';
import {VHeader} from "./vui/header/VHeader";
import './vui/css/css.utils.css'
import {$event, $stage, setApp} from "./index";
import {VPage} from "./vui/VPage";
import './vui/css/adaptive.css';
import "./vui/css/theme.light.scss";
import "./vui/css/theme.dark.scss";
import Spin from "antd/es/spin";
import {Card} from "antd";
import BOOT from './VGitBootstrap';
import {TOOLZ} from "./api/api";

class App extends Component {
    static lastUpdate = 0;
    state = {
        showLoader: true,
        playAnimation: true,
        message: "Bootstrapping...",
    };

    constructor(props) {
        super(props);
        window.addEventListener("resize", () => {
            this.forceUpdate()
        });
        $event.on("app.update", () => {
            this.forceUpdate(() => {
                $event.emit("app.updated");
            })
        });
        BOOT();
    }

    componentDidMount() {
        setApp(this);
        $event.emit("app-mounted", this);
    }

    render() {

        if (this.state.showLoader) {
            return (

                <div className={"App " + $stage.ui.theme + " VFlex VFlex-Center App-Loading "}
                     style={{position: "absolute", width: '100%', height: "100%"}}>
                    <Spin/>

                    <Card className="App-Loading-Card" size={"small"}>
                        <h3>{this.state.message}</h3>
                    </Card>

                </div>
            )
        }

        const s = this.state.playAnimation;
        setTimeout(() => {
            this.state.playAnimation = false;
        }, 1000);

        return (
            <div className={"App " + $stage.ui.theme + " " + (TOOLZ.isWidthAdaptive("adaptive", ""))}>
                <div className={"App-BG"}/>
                <VHeader>

                </VHeader>
                <VPage>

                </VPage>
            </div>
        );
    }
}

export default App;
