import {$event, $manager, $stage} from "./index";

export default () => {

    const hide = (app) => {

        const a = document.getElementsByClassName("App-Loading");
        if (a.length > 0) {
            a[0].childNodes.forEach(gg => {
                gg.classList.add("animated");
                gg.classList.add("fadeOut");
            });
            setTimeout(() => {
                app.setState({
                    showLoader: false
                })
            }, 1100)
        }

    };

    $event.on("app-mounted", (app) => {
        app.setState({
            message: "Connecting to server..."
        });

        $manager.server.ping(response => {
            if (!response.ok) {
                app.setState({
                    message: "Failed to connect\n" + response.asError().message
                })
            } else {
                if (!$stage.app.token) {
                    app.setState({
                        message: "Connected"
                    });
                    console.log($stage.app.account);
                    hide(app);
                } else {

                    app.setState({
                        message: "Logging in..."
                    });

                    $manager.user.getByToken($stage.app.token, response1 => {
                        if (!response1.ok) {
                            $stage.app.token = undefined;
                        } else {
                            $stage.app.account = response1.asUser();
                            $event.emit("update_info", true);
                        }
                        app.forceUpdate();
                        hide(app);

                    })
                }
            }
        })
    });
}