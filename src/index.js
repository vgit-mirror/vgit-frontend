import React from 'react';
import ReactDOM from 'react-dom';
import './vui/css/index.css';
import App from './App';
import 'antd/dist/antd.css';
import * as serviceWorker from './serviceWorker';
import onChange from 'on-change';
import {message} from "antd";
import Cookies from 'js-cookie';
import ee from 'event-emitter';
import 'animate.css/animate.min.css'
import API from "./api/api";
import './vui/css/v.utils.scss'
import {VGitAPIManager} from "./api/VGitAPIManager";
import {runUtils} from "./utils/VGitUtils.js";

export const $manager = new VGitAPIManager();

export function isUndefined(obj) {
    if (obj === undefined) {
        return true;
    }

    if (obj === 'undefined') {
        return true;
    }
    return false;
}

export function toUndefined(obj) {
    if (!obj) return undefined;
    if (obj === 'undefined') {
        return undefined;
    }
    return obj;
}

let p = "main";
const parts = window.location.href.split("/");
if (toUndefined(parts[3]) === undefined || parts[3] === '') {
    p = "main";
} else {
    p = parts[3];
}
const token = toUndefined(API.getCookie("token", undefined));
const theme = toUndefined(API.getCookie("theme", 'dark'));
export const $event = ee();
export const $stage = onChange({

    ui: {
        pages: {
            auth: {
                login: false,
                password: false,
            }
        },
        page: p,
        theme: theme
    },

    app: {
        token: token,
        account: undefined,
        repos: undefined,
        reposPaths: {}
    }

}, function (path, value, previousValue) {
    if ($app !== undefined) {
        $app.forceUpdate();
    }
    $event.emit("change." + path, value, previousValue);
    $event.emit("change", path, value, previousValue);
});

export let $app = undefined;

export function setApp(app) {
    $app = app;
}

export const $temp = {
    pages: {
        auth: {
            login: false,
            reg: false,
        }
    }
};
window.$stage = $stage;

ReactDOM.render(<App/>, document.getElementById('root'));

serviceWorker.unregister();
runUtils();
$event.on("change.ui.theme", () => {

    Cookies.set("theme", $stage.ui.theme);
});
$event.on("change.app.token", () => {

    Cookies.set("token", $stage.app.token);
    if (!!$stage.app.token) {
        message.info("Session saved");
    } else {
        message.error("Session destroyed")
    }
    $stage.app.repos = undefined;
    $stage.app.reposPaths = {};
    $stage.app.account = undefined;

    API.updateMe(() => {
        API.updateRepos(() => {

        })
    })
});


export const $LANGS = {
    "js": "javascript"
};
