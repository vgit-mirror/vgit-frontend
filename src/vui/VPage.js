import React from "react";
import Result from "antd/es/result";
import Button from "antd/es/button";
import {$stage} from "../index";
import {VMain} from "./VMain";
import {VRepoNew} from "./repo-new/VRepoNew";
import {VRepo} from "./repo/VRepo";

/* eslint-disable */
export class VPage extends React.Component {

    componentDidUpdate(prevProps, prevState, snapshot) {

        const fff = document.getElementById("content");
        const ffff = document.getElementById("subContent");
        if (!!ffff)
            ffff.className = "animated fadeIn animatedFast";
        if (!!fff)
            fff.className = "animated fadeIn animatedFast";
    }
    componentDidMount() {

        const fff = document.getElementById("content");
        const ffff = document.getElementById("subContent");
        if (!!ffff)
            ffff.className = "animated fadeIn animatedFast";
        else if (!!fff)
            fff.className = "animated fadeIn animatedFast";


    }

    constructor(props) {
        super(props);


    }

    render() {


        let con;

        switch ($stage.ui.page) {
            case "main":
                con = <VMain/>;
                break;
            case "repository-new":
                con = <VRepoNew/>;
                break;

            case "repo":
                con = <VRepo/>;
                break;

            default:
                con = (<div className="VFlex VFlex-Center VContent"><Result
                    status="error"
                    title="404"
                    subTitle="Sorry, the page you visited does not exist."
                    extra={<Button type="primary">Back Home</Button>}
                /></div>);
                break;
        }

        return <div id="content">
            <div className="VPage VContent">
                {con}
            </div>
        </div>
    }
}
