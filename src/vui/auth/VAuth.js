import React from "react";
import './auth.css'
import Card from "antd/es/card";
import {Input, Modal} from "antd";
import Button from "antd/es/button";
import {$event, $stage} from "../../index";
import Checkbox from "antd/es/checkbox";
import {message} from "antd/es";
import API from "../../api/api";
import Switch from "antd/es/switch";
import Tabs from "antd/es/tabs";

/* eslint-disable */
export class VAuth extends React.Component {
    render() {
        return <div className="VContent VAuth">
            <h1>Welcome to the VGit</h1>
            <Card>
                <Tabs defaultActiveKey="1" style={{width: 300}}>
                    <Tabs.TabPane tab="Login" key="1" style={{width: 300}}>

                        <Card bordered style={{width: 300}}>
                            <div className="">
                                <Input ref="l" type="login" placeholder="Login" className="VSize-MarginTB-6px"/>
                                <Input ref="p" type="password" placeholder="Password" className="VSize-MarginTB-6px"/>
                                <div className="VFlex-Row VSize-MarginTB-6px">
                                    <Checkbox
                                    >
                                        Save me
                                    </Checkbox>
                                    <Button
                                        type="primary"
                                        loading={$stage.ui.pages.auth.login}
                                        onClick={() => {
                                            $stage.ui.pages.auth.login = true;

                                            const l = this.refs["l"].state.value;
                                            const p1 = this.refs["p"].state.value;

                                            API.method("user.login", {
                                                login: l,
                                                password: p1
                                            }, (d) => {
                                                $stage.ui.pages.auth.login = false;
                                                if (!!d.error) {
                                                    Modal.error({
                                                        title: "Login failed (" + d.error.code + ")",
                                                        content: d.error.message
                                                    })
                                                } else {
                                                    $stage.app.token = d.token;
                                                }
                                            })
                                        }}
                                    >
                                        Login
                                    </Button>
                                </div>
                            </div>
                        </Card>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Register" key="2" style={{width: 300}}>

                        <Card bordered style={{width: 300}}>
                            <div>
                                <Input ref="reg-l" type="login" placeholder="Login" className="VSize-MarginTB-6px"/>
                                <Input ref="reg-n" type="name" placeholder="Full Name" className="VSize-MarginTB-6px"/>
                                <Input ref="reg-p1" type="password" placeholder="Password"
                                       className="VSize-MarginTB-6px"/>
                                <Input ref="reg-p2" type="password" placeholder="Repeat Password"
                                       className="VSize-MarginTB-6px"/>
                                <Input ref="reg-e" type="email" placeholder="Email" className="VSize-MarginTB-6px"/>
                                <div className="VFlex-Row VSize-MarginTB-6px">
                                    <Checkbox
                                    >
                                        Save me
                                    </Checkbox>
                                    <Button
                                        loading={$stage.ui.pages.auth.register}
                                        type="primary"
                                        onClick={() => {
                                            $stage.ui.pages.auth.register = true;
                                            const l = this.refs["reg-l"].state.value;
                                            const n = this.refs["reg-n"].state.value;
                                            const p1 = this.refs["reg-p1"].state.value;
                                            const p2 = this.refs["reg-p2"].state.value;
                                            const e = this.refs["reg-e"].state.value;

                                            API.method("user.registration", {
                                                login: l,
                                                email: e,
                                                password: p1,
                                                fullName: n
                                            }, (d) => {
                                                $stage.ui.pages.auth.register = false;

                                                if (!!d.error) {
                                                    Modal.error({
                                                        title: "Registration failed (" + d.error.code + ")",
                                                        content: d.error.message
                                                    })
                                                } else {
                                                    API.method("user.login", {
                                                        login: l,
                                                        password: p1
                                                    }, (d) => {
                                                        $stage.ui.pages.auth.login = false;
                                                        if (!!d.error) {
                                                            Modal.error({
                                                                title: "Login failed (" + d.error.code + ")",
                                                                content: d.error.message
                                                            })
                                                        } else {
                                                            $stage.app.token = d.token;
                                                            message.loading('Loading profile...', 10)
                                                        }
                                                    })
                                                }
                                            })
                                        }}
                                    >
                                        Register
                                    </Button>
                                </div>
                            </div>
                        </Card>

                    </Tabs.TabPane>
                </Tabs>

            </Card>
            <Card title="Change Theme" bordered className="VSize-margin-10px trans" style={{width: 350}}>
                <Switch checkedChildren="Dark" unCheckedChildren="Light" onChange={(a) => {
                    $stage.ui.theme = a ? "dark" : "light";
                    $event.emit("app.update");
                }} defaultChecked={$stage.ui.theme === "dark"}/>
            </Card>
        </div>
    }
}