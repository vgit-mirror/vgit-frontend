import {VRepoElement} from "../VRepoElement";
import React from "react";
import {Col, Row} from 'react-flexbox-grid';
import Divider from "antd/es/divider";
import {TOOLZ} from "../../../api/api";
import Icon from "antd/es/icon";
import moment from "moment";
import Button from "antd/es/button";
import {Tooltip} from "antd";

export default class VRepoFileTable extends VRepoElement {

    dClick = false;

    createRow(file) {

        const name = (
            <div className={"VFlex VFlex-Row VFlex-Center-V "}>

                {file.isDirectory ? <Icon type="folder" theme="filled"
                                          style={{width: '14px', height: '14px'}}/> :
                    <Icon type="file" theme="filled" style={{width: '14px', height: '14px'}}/>}
                <h2 style={{
                    marginLeft: "6px",
                    fontSize: '14px',
                    marginBottom: 0,
                    fontWeight: 'bold',
                    textAlign: 'left'
                }} className="VText-Overflow-e">{file.name}</h2>

            </div>
        );
        const date = (

            <div style={{textAlign: "right"}}>
                {moment(file.commit.time * 1000).fromNow()}
            </div>

        );

        return TOOLZ.isWidthAdaptive(
            (

                <Row style={{padding: '12px 0'}}
                     className={"VFileTable-Row " + (!!this.props.animated ? "animated fadeIn" : "")} onClick={() => {
                    if (this.dClick) {
                        this.dClick = false;
                        return;
                    }
                    if (!!this.props.row.onClick) {
                        this.props.row.onClick(file);
                    }
                }}>
                    <Col xs className="VText-Overflow-e VFlex VFlex-Row VFlex-Center-V" style={{fontWeight: 'bold'}}>
                        {name}
                    </Col>
                    <Col xs={1} onClick={() => {
                        this.dClick = true;
                    }}>
                        <Tooltip title={file.commit.message} onClick={() => {
                            this.dClick = true;
                        }}>
                            <Button icon="info" size={2} onClick={() => {
                                this.dClick = true;
                            }}/>
                        </Tooltip>
                    </Col>
                    <Col xs={2} className="VText-Overflow-e" style={{textAlign: 'right', minWidth: '120px'}}>
                        {date}
                    </Col>
                </Row>
            ),

            (

                <Row style={{padding: '12px 0'}}
                     className={"VFileTable-Row " + (!!this.props.animated ? "animated fadeIn" : "")} onClick={() => {
                    if (!!this.props.row.onClick) {
                        this.props.row.onClick(file);
                    }
                }}>
                    <Col xs={3} className="VText-Overflow-e" style={{fontWeight: 'bold'}}>
                        {name}
                    </Col>
                    <Col xs className="VText-Overflow-e" style={{textAlign: 'left'}}>
                        <h2 style={{
                            marginLeft: "6px",
                            fontSize: '12px',
                            textAlign: 'right',
                            marginBottom: 0
                        }} className="VText-Overflow-e">{file.commit.message}</h2>
                    </Col>
                    <Col xs={2} className="VText-Overflow-e" style={{textAlign: 'right'}}>
                        {date}
                    </Col>
                </Row>
            )
        )
    }

    render() {
        const rows = [];
        let id = 0;
        for (const fId in this.props.files) {
            const file = this.props.files[fId];
            const sep = ((id + 1) !== this.props.files.length);
            rows.push(this.createRow(file));
            if (sep) {
                rows.push(<Divider style={{margin: 0}}/>)
            }
            id++;
        }
        return (
            <div>

                {rows}
            </div>
        );
    }
}