import React from 'react';
import "../../../utils/PrismJSImportLangs"
import Prism from 'prismjs';

export default class VRepoCodeViewer extends React.Component {
    state = {
        code: ['function x() {', "  console.log('Hello world!');", '}'].join('\n'),
        h: 0,
        lang: 'javascript'
    };

    constructor(props) {
        super(props);
        if (!!props.txt) {
            this.state.code = props.txt;
        }
        if (!!this.props.lang) {
            this.state.lang = this.props.lang;
        }
    }

    componentDidMount() {
        Prism.highlightAll();
    }

    render() {
        return (
            <pre className={"language-" + this.state.lang + " line-numbers"}><code
                className={"language-" + this.state.lang}>{this.state.code}</code></pre>
        )
    }
}