import React from "react";
import {Affix} from "antd";

export class VRepoSider extends React.Component {

    render() {
        return (
            <Affix
                offsetTop={48}
                className={"VRepo-Sider-Affix"}
            >
                <div className={"VRepo-Sider " + (this.props.collapsed ? "hide" : "show")}>
                    {this.props.children}
                </div>
            </Affix>
        );
    }

}