import {VRepoElement} from "../VRepoElement";
import React from "react";
import {Card, message, Modal, Spin} from "antd";
import API from "../../../api/api";
import Icon from "antd/es/icon";
import "../../../utils/PrismJSImportLangs"
import Prism from 'prismjs';
import ReactHtmlParser from 'react-html-parser';
import markdownIt from 'markdown-it'
import VRepoFileTable from "./VRepoFileTable";
import {Breadcrumb} from "antd/es";
import VRepoCodeViewer from "./VRepoCodeViewer";

export default class VRepoFileBrowser extends VRepoElement {

    features = {
        gotoFolder: true,
        openFile: true,
        markdown: true
    };
    markdown = undefined;
    files = undefined;
    fileName = undefined;
    file = undefined;
    lang = undefined;
    path = "/";
    isLoading = false;
    state = {
        fileBrowser: this
    };
    loadedPath = "";
    stepLoader = {
        0: () => {
            return (
                <div style={{width: '100%', textAlign: 'center'}}>
                    <Spin indicator={<Icon className type="loading" style={{fontSize: 24}} spin/>}/>
                    <h3>Loading...</h3>
                </div>
            )
        },
        1: () => {

            const crumbs = [];
            const items = this.path.split("/");
            let tmpPath = "";
            for (const iId in items) {
                const item = items[iId];
                tmpPath = tmpPath + item + "/";
                const path = tmpPath;
                if (path !== "" && item !== "") {
                    crumbs.push(
                        <Breadcrumb.Item>
                            <a onClick={() => {
                                this.path = path;
                                this.getFiles();
                            }}>{item}</a>
                        </Breadcrumb.Item>
                    )
                }
            }
            /*
             onClick={() => {
                                            this.path="/";
                                            this.getFiles();
                                        }}
             */
            return (

                <div className="VFileTable-Clickable">
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <a onClick={() => {
                                this.path = "/";
                                this.getFiles();
                            }}>{this.repo.repo.name}</a>
                        </Breadcrumb.Item>
                        {crumbs}
                    </Breadcrumb>
                    <Spin spinning={this.isLoading}>
                        <VRepoFileTable animated={this.props.animated} files={this.files} row={{
                            onClick: (f) => {
                                if (f.isDirectory) {
                                    this.path = f.path;
                                    this.getFiles();
                                } else {
                                    if (f.size < 15000) {
                                        this.isLoading = true;
                                        this.forceUpdate();

                                        API.getFileContent(this.repo.path, f.path, (con, ok) => {
                                            this.isLoading = false;
                                            if (ok) {
                                                const nParts = f.name.split(".");
                                                this.file = con;
                                                this.lang = API.getLang(f.name);
                                                this.fileName = f;
                                            } else {
                                                message.warn("Failed to load file")
                                            }
                                            this.forceUpdate();
                                        })
                                    } else {
                                        Modal.confirm({
                                            title: "Open file?",
                                            content: "File too big, you really want open it?",
                                            okType: 'danger',
                                            okText: "Open",
                                            onOk: () => {
                                                message.warn("File is opening...");
                                                this.isLoading = true;
                                                this.forceUpdate();

                                                API.getFileContent(this.repo.path, f.path, (con, ok) => {
                                                    this.isLoading = false;
                                                    if (ok) {
                                                        this.file = con;
                                                        this.lang = API.getLang(f.name);
                                                        this.fileName = f;
                                                    } else {
                                                        message.warn("Failed to load file")
                                                    }
                                                    this.forceUpdate();
                                                })
                                            }
                                        })
                                    }
                                }
                            }
                        }}/>
                    </Spin>
                </div>
            )
        },
        2: () => {
            return (
                <div style={{width: '100%'}}>
                    <Spin indicator={<Icon className type="loading" style={{fontSize: 24}} spin/>}/>
                    <h3>Loading...</h3>
                </div>
            )
        },
        3: () => {
            return (
                <div style={{width: '100%'}}>
                    <Icon type="warning"/>
                    <h2>Failed load file tree</h2>
                </div>
            )
        }
    };
    step = 0;

    constructor(props) {
        super(props);
        this.path = props.path || "/";
        if (!!props.features) {
            for (const featureId in props.features) {
                this.features[featureId] = props.features[featureId];
            }
        }
    }

    componentDidMount() {
        Prism.highlightAll();
    }

    getFiles() {
        this.markdown = undefined;
        this.isLoading = true;
        this.forceUpdate();
        API.getTree(this.repo.path, this.path, (response) => {
            if (!!response.error) {
                this.step = 3;
            } else {
                const files = [];
                for (const f in response) {
                    const file = response[f];

                    if (file.name.toLowerCase() === "readme.md") {
                        if (!!this.features.markdown) {
                            API.getFileContent(this.repo.path, file.path, (res, bool, xhr) => {
                                if (bool) {
                                    const i = new markdownIt({
                                        highlight: function (str, lang) {
                                            return '<pre class="language-' + lang + '"><code>' + str + '</code></pre>';
                                        }
                                    });
                                    this.markdown = (
                                        <Card style={{textAlign: "left", marginTop: '12px'}}
                                              className={"VRepo-Content-Card VFlex"}
                                              title={file.name}>
                                            {ReactHtmlParser(i.render(res))}
                                        </Card>
                                    );
                                }
                            });
                        }
                    }

                    files.push(file)
                }
                this.files = files;
                this.step = 1;
            }
            this.loadedPath = this.path;
            this.isLoading = false;
            this.forceUpdate()
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        Prism.highlightAll();
        if (!!this.repo.repo) {
            if (this.loadedPath !== this.path) {
                this.repo.title = this.repo.repo.name;
                if (!this.isLoading) {
                    this.step = 2;
                    this.isLoading = true;
                    this.files = undefined;
                    this.getFiles()
                }
            }
        }
    }

    editorDidMount = (editor, monaco) => {
        this.editor = editor;
        editor.focus();
    };

    render() {
        let aContent = [];
        const content = this.stepLoader[this.step]();

        if (!!this.markdown) {
            aContent.push(
                this.markdown
            )
        }

        if (this.file !== undefined) {

            const crumbs = [];
            const items = this.path.split("/");
            let tmpPath = "";
            for (const iId in items) {
                const item = items[iId];
                tmpPath = tmpPath + item + "/";
                const path = tmpPath;
                if (path !== "" && item !== "") {
                    crumbs.push(
                        <Breadcrumb.Item>
                            <a onClick={() => {
                                this.file = undefined;
                                this.fileName = undefined;
                                this.path = path;
                                this.getFiles();
                            }}>{item}</a>
                        </Breadcrumb.Item>
                    )
                }
            }

            const b = (

                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a onClick={() => {
                            this.file = undefined;
                            this.fileName = undefined;
                            this.path = "/";
                            this.getFiles();
                        }}>{this.repo.repo.name}</a>
                    </Breadcrumb.Item>
                    {crumbs}
                    <Breadcrumb.Item>
                        <a>{this.fileName.name}</a>
                    </Breadcrumb.Item>
                </Breadcrumb>


            );

            return (

                <div style={{width: '100%'}}>

                    <Card style={{textAlign: "left", marginTop: '12px'}} className={"VRepo-Content-Card"}
                          bodyStyle={{minHeight: '600px'}}>
                        {b}
                        <div className="animated fadeIn">
                            <VRepoCodeViewer txt={this.file} lang={this.lang}>

                            </VRepoCodeViewer>
                        </div>
                        {b}
                    </Card>
                </div>

            )
        }

        return (
            <div style={{width: '100%'}}>

                <Card style={{textAlign: "left", marginTop: '12px'}} className={"VRepo-Content-Card VFlex"}>
                    {content}
                </Card>

                {aContent}
            </div>
        );
    }
}
