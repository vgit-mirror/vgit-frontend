import React from "react";
import {VRepoElement} from "../VRepoElement";
import {Spin} from "antd";
import Icon from "antd/es/icon";
import {TOOLZ} from "../../../api/api";

export class VRepoHeader extends VRepoElement {
    render() {

        let name;
        if (!!this.repo.repo) {
            name = this.repo.repo.name;
        } else {
            name = <Spin indicator={<Icon className="whiteIcon" type="loading" style={{fontSize: 24}} spin/>}/>
        }
        let icon;
        if (!!this.repo.state.sider) {
            icon = <Icon type="caret-right"/>


        } else {
            icon = <Icon type="caret-left"/>
        }
        return (
            <div style={{
                width: '100%',
                height: '48px',
                color: "white",
                display: 'flex',
                zIndex: '2060',
                alignItems: "center",
                justifyContent: 'flex-start',
                flexDirection: 'row'
            }} className="VHeader-Sub">
                {TOOLZ.isWidthAdaptive(
                    <div style={{marginLeft: '12px'}}
                         className={"VHeader-Icon trans " + (!this.repo.state.sider ? "VHide-Mini" : "")}
                         onClick={() => {
                             this.repo.setState({
                                 sider: !this.repo.state.sider
                             })
                         }}>
                        {icon}
                    </div>, <span/>)}

                <div style={{marginLeft: '12px', fontWeight: "bold", fontSize: '18px'}}>
                    {name}
                </div>
            </div>
        );
    }
}

export class VRepoSiderHeader extends VRepoElement {
    render() {

        let icon;
        if (!!this.repo.state.sider) {
            icon = <Icon type="caret-right"/>


        } else {
            icon = <Icon type="caret-left"/>
        }

        //VHide-Mini
        return (

            <div style={{
                width: '100%',
                height: '48px',
                color: "white",
                display: 'flex',
                alignItems: "center",
                justifyContent: 'flex-start',
                flexDirection: 'row'
            }} className="VHeader-Sub">


                {TOOLZ.isWidthAdaptive(
                    <div style={{marginLeft: '12px'}}
                         className={"VHeader-Icon trans " + (!this.repo.state.sider ? "VHide-Mini" : "")}
                         onClick={() => {
                             this.repo.setState({
                                 sider: !this.repo.state.sider
                             })
                         }}>
                        {icon}
                    </div>, <span/>)}

            </div>
        );
    }
}