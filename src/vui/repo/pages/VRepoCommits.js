import React from 'react';
import {VRepoElement} from "../VRepoElement";
import {Card, Pagination, Tooltip} from "antd";
import {Spin} from "antd/es";
import API, {TOOLZ} from "../../../api/api";
import Gravatar from "../../../utils/Gravatar";
import moment from "moment";

/**
 *
 * Created at: 10:57 21.09.2019
 *  -> By: themr
 *
 */

export class VRepoCommits extends VRepoElement {

    isLoading = false;
    step = 0;
    page = 1;
    commits = [];


    constructor(props) {
        super(props);
        this.repo.eventEmitter.once("page_change_commits", (page, data) => {
            this.forceUpdate();
            this.afterInit();
        });
        this.repo.eventEmitter.once("updated_commits_count", (data) => {
            this.afterInit()
        })
    }


    fetchCommits(page) {
        if (!this.isLoading) {
            if (!!this.repo.totalCommits) {
                this.isLoading = true;
                this.forceUpdate();
                this.page = page;
                const offset = page * 20 - 20;
                let count = this.repo.totalCommits - offset;
                if (count > 20) {
                    count = 20;
                }

                API.getCommits(this.repo.path, count, offset, (ok, c) => {
                    if (!ok) {
                        this.step = 3;
                    } else {
                        this.commits = c;
                        this.step = 2;
                    }
                    this.isLoading = false;
                    this.forceUpdate()
                })
            }
        }
    }

    afterInit() {
        this.fetchCommits(1);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.step === 0 && this.isLoading === false) {
            this.fetchCommits(this.page);
        }
    }


    render() {
        if (this.step === 3) {
            return (
                <div>
                    <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                        <h2 style={{textAlign: "center"}}>Critical error</h2>
                    </Card>
                </div>
            )
        }
        if (this.step === 2) {

            const commits = [];

            for (const cId in this.commits) {
                const commit = this.commits[cId];
                commits.push(
                    <Card size="small" style={{marginTop: '6px'}} className="VRepo-Content-Card VRepo-Commit-Card">
                        <div className="VFlex VFlex-Row" style={{justifyContent: "space-between", margin: '0px 6px'}}>
                            <div className="VFlex VFlex-Row VFlex-Center-V" style={{minWidth: 0}}>
                                <Tooltip title={commit.author.name}>
                                    <Gravatar email={commit.author.email} size={32} style={{
                                        minWidth: '32px',
                                        minHeight: '32px'
                                    }}/>
                                </Tooltip>
                                <pre style={{marginLeft: '10px', marginBottom: 0}}
                                     onClick={() => {
                                         this.repo.changePage("diffs/" + commit.name)
                                     }}>
                                        {commit.fullMessage}
                                </pre>
                            </div>
                            <div>
                                <Tooltip title={moment(commit.time * 1000).format("HH:mm L")}>
                                    <div className="VFlex"
                                         style={{justifyContent: "flex-end", textAlign: 'right'}}>
                                        <div>
                                            {moment(commit.time * 1000).fromNow()}
                                        </div>
                                        <a onClick={() => {
                                            TOOLZ.copy(commit.name);
                                        }} className="primary">
                                            {commit.name.substr(0, 10)}
                                        </a>
                                    </div>
                                </Tooltip>
                            </div>
                        </div>
                    </Card>
                )
            }

            const p = (
                <Pagination
                    pageSize={20}
                    className="VFlex VFlex-Row VFlex-Center"
                    total={this.repo.totalCommits}
                    current={this.page}
                    style={{marginTop: '12px', marginBottom: '12px'}}
                    onChange={(page, pageSize) => {
                        this.fetchCommits(page);
                        this.forceUpdate();
                    }}
                />
            );

            return (

                <div>

                    <Card style={{textAlign: "left"}} className={"VRepo-Content-Card VRepo-Commit-Root"}>
                        {p}
                        <Spin spinning={this.isLoading}>
                            {commits}
                        </Spin>
                        {p}
                    </Card>
                </div>

            )
        }
        if (!this.repo.totalCommits) {
            return (
                <div>
                    <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                        <h2 style={{textAlign: "center"}}><Spin/> Loading total commits</h2>
                    </Card>
                </div>
            );
        }
        return (
            <div>

                <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                    <h2 style={{textAlign: "center"}}><Spin/> Pending...</h2>
                </Card>
            </div>
        );
    }
}