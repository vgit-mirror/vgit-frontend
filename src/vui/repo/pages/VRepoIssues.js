import React from 'react';
import {VRepoElement} from "../VRepoElement";
import {Card} from "antd";
import {Spin} from "antd/es";

/**
 *
 * Created at: 10:55 21.09.2019
 *  -> By: themr
 *
 */

export class VRepoIssues extends VRepoElement {


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!!this.repo.repo) {

        }
    }

    render() {
        return (
            <div>

                <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                    <h2 style={{textAlign: "center"}}><Spin/> Pending...</h2>
                </Card>
            </div>
        );
    }
}