import * as React from "react";
import {Card, Tooltip} from "antd";
import {VRepoElement} from "../VRepoElement";
import Gravatar from "../../../utils/Gravatar";
import moment from "moment";
import VRepoFileBrowser from "../elements/VRepoFileBrowser";

export class VRepoMain extends VRepoElement {
    constructor(props) {
        super(props);
        this.repo.eventEmitter.once("page_change_main", (page, data) => {
            this.forceUpdate();
            this.afterInit();
        })
    }

    afterInit() {
        this.repo.updateLastCommit();
    }

    render() {

        return (
            <div>
                <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                    <h2 style={{fontWeight: 'bold'}}>{this.ifNonGetLoader(this.Repo(), () => this.repo.repo.name)}</h2>
                    <h3>{this.ifNonGetLoader(this.Repo(), () => this.repo.repo.description)}</h3>
                    {this.ifNonGetLoader(this.repo.lastCommit, (obj) => {
                        if (!!obj.author) {
                            return (
                                <Card size="small">
                                    <div className="VFlex VFlex-Row" style={{justifyContent: "space-between"}}>

                                        <div className="VFlex VFlex-Row VFlex-Center-V" style={{minWidth: 0}}>

                                            <Tooltip title={obj.author.name}>
                                                <Gravatar email={obj.author.email} size={32} style={{
                                                    minWidth: '32px',
                                                    minHeight: '32px'
                                                }}/>
                                            </Tooltip>
                                            <Tooltip title={"Show difference"}>
                                                <a style={{
                                                    marginLeft: "6px",
                                                    marginBottom: 0,
                                                    fontWeight: 'bold'
                                                }} className="VText-Overflow-e">
                                                    {obj.message}
                                                </a>
                                            </Tooltip>
                                        </div>
                                        <div>
                                            <Tooltip title={moment(obj.time * 1000).format("HH:mm L")}>
                                                <div className="VFlex"
                                                     style={{justifyContent: "flex-end", textAlign: 'right'}}>
                                                    <div>
                                                        {moment(obj.time * 1000).fromNow()}
                                                    </div>
                                                    <div>
                                                        {obj.name.substr(0, 10)}
                                                    </div>
                                                </div>
                                            </Tooltip>
                                        </div>
                                    </div>
                                </Card>
                            )
                        } else {
                            return (
                                <span/>
                            )
                        }
                    })}
                </Card>
                <VRepoFileBrowser repo={this.repo} animated={true}/>
            </div>
        );
    }
}