import {VRepoElement} from "../VRepoElement";
import {Card} from "antd";
import React from "react";

export class VRepoArtifacts extends VRepoElement {
    render() {
        return (
            <div>
                <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                    <h2>Artifacts are empty</h2>
                </Card>
            </div>
        )
    }
}