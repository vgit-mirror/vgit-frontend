import React from 'react';
import {VRepoElement} from "../VRepoElement";
import {Card, Input, message, Modal, notification} from "antd";
import {Spin} from "antd/es";
import Icon from "antd/es/icon";
import Button from "antd/es/button";
import ButtonGroup from "antd/es/button/button-group";
import API from "../../../api/api";

/**
 *
 * Created at: 10:58 21.09.2019
 *  -> By: themr
 *
 */

export class VRepoBranches extends VRepoElement {

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    showNewDialog() {
        let f = "";
        Modal.confirm({
            title: "Create new branch",
            content: (
                <div>
                    <Input placeholder="Input branch name" onChange={(e) => {
                        f = e.target.value;
                    }}/>
                </div>
            ),
            onOk: () => {
                if (f === "") {
                    message.warn("Canceled. Name is empty");
                    return;
                }
                API.newBranch(this.repo.path, f, (ok, data) => {
                    if (!!ok) {
                        notification.success({
                            message: "New branch",
                            description: "Branch '" + f + "' created"
                        });
                        this.forceUpdate();
                        this.repo.update();
                    } else {

                        notification.error({
                            message: "Failed create branch (" + data.code + ")",
                            description: data.message
                        });
                    }
                })

            }
        })
    }

    render() {
        if (!!this.repo.branches) {
            const branches = [];
            for (const bId in this.repo.branches) {
                const b = this.repo.branches[bId];
                branches.push(
                    <Card size={"small"} style={{textAlign: 'left', marginTop: '6px', marginBottom: '6px'}}>
                        <div className="VFlex VFlex-Center-V VFlex-Row" style={{justifyContent: 'space-between'}}>
                            <div className="VFlex VFlex-Row VFlex-Center-V">
                                <h2 style={{marginBottom: 0}}>{b.substr(11)}</h2>
                                {this.repo.defaultBranch === b ?
                                    <Icon type={"star"} theme="filled" style={{marginLeft: '6px'}}
                                          className="animated bounceIn"/> : <span/>}
                            </div>
                            <div>

                                {this.repo.defaultBranch !== b ?
                                    <ButtonGroup>
                                        <Button type="primary" onClick={() => {
                                            API.setDefBranch(this.repo.path, b, (ok, data) => {

                                                const m = message.loading("Setting branch " + b.substr(11) + " as default", 0);
                                                API.setDefBranch(this.repo.path, b, (ok, data) => {
                                                    m();
                                                    if (ok) {
                                                        message.success("Branch '" + b.substr(11) + "' set as default");
                                                        this.forceUpdate();
                                                        this.repo.update();
                                                    } else {

                                                        notification.error({
                                                            message: "Failed set as default (" + data.code + ")",
                                                            description: data.message
                                                        });
                                                    }
                                                })
                                            })
                                        }}>
                                            <Icon type="star"/>
                                        </Button>
                                        <Button type="danger" onClick={() => {
                                            Modal.confirm({
                                                title: "Delete " + b.substr(11) + "?",
                                                okType: "danger",
                                                onOk: () => {
                                                    const m = message.loading("Removing...", 0);
                                                    API.removeBranch(this.repo.path, b, (ok, data) => {
                                                        m();
                                                        if (ok) {
                                                            message.success("Branch '" + b.substr(11) + "' removed");
                                                            this.forceUpdate();
                                                            this.repo.update();
                                                        } else {

                                                            notification.error({
                                                                message: "Failed delete branch (" + data.code + ")",
                                                                description: data.message
                                                            });
                                                        }
                                                    })
                                                }
                                            })
                                        }}>
                                            <Icon type="close"/>
                                        </Button>
                                    </ButtonGroup>
                                    :
                                    <span/>
                                }
                            </div>
                        </div>
                    </Card>
                )
            }

            const title = (
                <div className="VFlex VFlex-Row VFlex-Center">
                    <h3 style={{marginBottom: 0}}>Branches</h3>
                    <Button size={"small"} shape={"circle-outline"} style={{marginLeft: "6px"}} onClick={() => {
                        this.showNewDialog();
                    }}>
                        <Icon type="plus"/>
                    </Button>
                </div>
            );

            return (
                <div>

                    <Card title={title} size={"small"}>

                        {branches}
                    </Card>

                </div>
            );

        } else {
            return (
                <div>

                    <Card title="Branches">
                        <h2 style={{textAlign: "center"}}><Spin/> Pending...</h2>
                    </Card>

                </div>
            );
        }
    }
}