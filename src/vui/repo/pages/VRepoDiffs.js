import React from "react";
import {VRepoElement} from "../VRepoElement";
import {Card} from "antd";
import API from "../../../api/api";
import Spin from "antd/es/spin";
import ReactDiffViewer from 'react-diff-viewer'
import Prism from 'prismjs';
import 'prismjs/themes/prism.css'
import 'prismjs/components'
import 'prismjs/components/prism-yaml'
import 'prismjs/components/prism-bash'
import 'prismjs/components/prism-markup-templating'
import 'prismjs/components/prism-markup'
import 'prismjs/components/prism-php.min'
import VRepoCodeViewer from "../elements/VRepoCodeViewer";
import 'prismjs/plugins/line-numbers/prism-line-numbers.css'
import 'prismjs/plugins/line-numbers/prism-line-numbers.js'
import applyLineNumber from '../../../utils/PrismLineNumberPlugin';
import styles from '../utils/VRepoDiffViewerStyle'
import {$stage} from "../../../index";


export default class VRepoDiffs extends VRepoElement {

    state = {
        commit: undefined,
        data: undefined
    };
    isPending = false;
    loaded = undefined;
    isError = false;

    constructor(props) {
        super(props);
        this.state.commit = this.props.commit;
        this.repo.eventEmitter.once("page_change_diffs", (p, d) => {
            this.isError = false;
            this.setState({
                commit: API.getParts(window.location.href)[7],
                data: undefined
            })


        })

    }

    refreshDiffs() {
        if (!!this.repo.repo && !this.isPending && !(this.loaded === this.state.commit)) {
            this.loaded = this.state.commit;
            this.isPending = true;
            API.getDiffs(this.repo.path, this.state.commit, (ok, data) => {
                this.isPending = false;
                if (!!ok) {
                    this.setState({
                        data: data
                    })
                } else {
                    this.isError = true;
                    this.forceUpdate();

                }
            })
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        Prism.highlightAll();
        this.refreshDiffs()
    }

    _404() {
        return (
            <div>
                <Card style={{textAlign: "left"}} className={"VRepo-Content-Card"}>
                    <h2>Commit not found :(</h2>
                </Card>
            </div>
        )
    }


    render() {
        if (this.isError) {
            return (
                <div>
                    <Card>
                        <h2>Sorry fatal error has crashed this wonderful application :C</h2>
                    </Card>
                </div>
            )
        }
        if (!this.state.commit || this.state.data === null) {
            return this._404();
        }
        if (!this.state.data || !this.repo.repo) {
            return (
                <div>
                    <Card style={{textAlign: "center"}} className={"VRepo-Content-Card"}>
                        <h2>Loading...</h2>
                        <Spin/>
                    </Card>
                </div>
            );
        }

        const list = [];
        for (const i in this.state.data) {
            const d = this.state.data[i];
            let head = (
                <div>???</div>
            );
            let content = (
                <div>???</div>
            );
            switch (d.changeType) {
                case("MODIFY"):
                    head = (
                        <div className="VFlex VFlex-Row VFlex-Center">
                            <a>{d.newPath.split("/")[d.newPath.split("/").length - 1]}</a>
                            <p style={{marginLeft: '10px', marginBottom: 0}}>
                                Modified
                            </p>
                        </div>
                    );
                    if (d.oldFile === null || d.newFile === null) {

                        content = (
                            <div>
                                <div style={{textAlign: 'center'}}>
                                    File too big <br/>
                                    <a>Open file</a>
                                </div>
                            </div>
                        );
                    } else {
                        console.log(Prism);
                        applyLineNumber(Prism);
                        content = (
                            <div>
                                <div style={{textAlign: 'left'}} className="diff-viewer">

                                    <ReactDiffViewer
                                        styles={styles[$stage.ui.theme]}
                                        oldValue={d.oldFile}
                                        newValue={d.newFile}
                                        splitView={false}
                                        renderContent={str => {

                                            applyLineNumber(Prism);
                                            return (<pre
                                                style={{display: 'inline'}}
                                                dangerouslySetInnerHTML={{__html: Prism.highlight(str, Prism.languages.javascript)}}
                                                data-enable-plugins="line-numbers"
                                            />)
                                        }}
                                    />

                                </div>
                            </div>
                        );
                    }
                    break;
                case("ADD"):
                    head = (

                        <div className="VFlex VFlex-Row VFlex-Center">
                            <a>{d.newPath.split("/")[d.newPath.split("/").length - 1]}</a>
                            <p style={{marginLeft: '10px', marginBottom: 0}}>
                                Added
                            </p>
                        </div>
                    );
                    content = (
                        <div>
                            <div style={{textAlign: 'left'}}>

                                <VRepoCodeViewer txt={d.newFile}>

                                </VRepoCodeViewer>

                            </div>
                        </div>
                    );
                    break;
            }
            list.push(
                <Card size={"small"} title={head} className="VRepo-Content-List-Card">
                    {content}
                </Card>
            )
        }
        return (

            <div>
                <Card style={{textAlign: "center"}} className={"VRepo-Content-Card VRepo-Content-List"}>
                    {list}
                </Card>
            </div>
        )
    }
}