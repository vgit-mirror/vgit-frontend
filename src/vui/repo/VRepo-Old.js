import * as React from "react";
import Card from "antd/es/card";
import {$LANGS, $stage, API} from "../../index";
import List from "antd/es/list";
import Icon from "antd/es/icon";
import Input from "antd/es/input";
import {Avatar, message, Modal} from "antd";
import Prism from 'prismjs'
import 'prismjs/themes/prism.css'
import {MonacoDiffEditor} from "react-monaco-editor";
import 'monaco-editor/dev/vs/loader.js'
import 'prismjs/components'
import 'prismjs/components/prism-yaml'
import 'prismjs/components/prism-bash'
import 'prismjs/components/prism-markup-templating'
import 'prismjs/components/prism-markup'
import 'prismjs/components/prism-php.min'
import ReactHtmlParser from 'react-html-parser';
import markdownIt from 'markdown-it'
import Layout from "antd/es/layout";
import Button from "antd/es/button";
import Popover from "antd/es/popover";
import Breadcrumb from "antd/es/breadcrumb";
import md5 from "md5";
import moment from "moment";
import SubMenu from "antd/es/menu/SubMenu";
import Menu from "antd/es/menu";
import Pagination from "antd/es/pagination";
import Spin from "antd/es/spin";
import Tag from "antd/es/tag";
import ButtonGroup from "antd/es/button/button-group";
import Tooltip from "antd/es/tooltip";
import CollapsePanel from "antd/es/collapse/CollapsePanel";
import Collapse from "antd/es/collapse";
import Table from "antd/es/table";

function reverseObject(object) {
    var newObject = {};
    var keys = [];

    for (var key in object) {
        keys.push(key);
    }

    for (var i = keys.length - 1; i >= 0; i--) {
        var value = object[keys[i]];
        newObject[keys[i]] = value;
    }

    return newObject;
}

export class VRepoOld extends React.Component {
    pages = {
        main: () => this.main(),
        commits: () => this.commits(),
        branches: () => this.branches(),
        diffs: () => this.diffs(),
        file: () => this.file(),
        files: () => this.tree()
    };

    constructor(props) {
        super(props);
        this.resetState();
    }

    resetState() {
        this.state = {
            id: -1,
            //path: API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5],
            files: undefined,
            page: undefined,
            isGettingFiles: false,
            isGettingCommits: false,
            lastUpdate: 0,
            currentPath: "/",
            repository: undefined,
            firstCommitName: undefined,
            commits: undefined,
            totalCommits: undefined,
            offset: 0,
            count: 20,
            commitList: [],
            range: [],
            cPage: 1,
            fileViewData: {
                path: "/",
                hasFile: null,
                files: undefined,
                isLoading: false,
                file: undefined,
                show: [
                    <Card style={{width: '80%', marginTop: '30px'}}>
                        {this.getLoadingIndicator()}
                    </Card>
                ]
            },
            pos: "-1--1",
            branches: undefined,
            defaultBranch: undefined,
            diffs: undefined,
            diffPath: "c1/c2"
        };

    }

    getBranches(cb) {

        const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
        API.getBranches(path, (ok, res) => {
            if (!!this.state.defaultBranch) {
                this.setState({
                    branches: res
                })
            } else {
                API.getDefaultBranch(path, (ok, b) => {


                    this.setState({
                        branches: res,
                        defaultBranch: b.branch
                    })
                })
            }
        })
    }

    getCommitsCount(cb) {

        if (!this.state.totalCommits) {
            API.getCommitsCount(this.state.repository.path, (ok, c) => {
                if (!ok) {
                    message.error("Failed load commits count");
                    c(undefined)
                } else {
                    cb(c.count);
                }
            })

        }

    }

    async updateCommits(count, offset, commitscb) {
        if (!!this.state.repository) {

            if (!this.state.isGettingCommits) {

                this.setState({
                    isGettingCommits: true
                });
                if (offset !== 0) {
                    offset--;
                }

                count++;
                API.getCommits(this.state.repository.path, count, offset, (ok, d) => {
                    if (ok) {
                        let commits = this.state.commits;
                        if (!commits) {
                            commits = {};
                        }
                        let firstCommitName = undefined;
                        if (d.length > 0)
                            firstCommitName = d[0].name;
                        for (const commitKey in d) {
                            const commit = d[commitKey];
                            commits[commit.name] = commit;
                        }

                        if (!!commitscb)
                            commitscb(d);

                        if (!this.state.totalCommits) {
                            this.getCommitsCount((c) => {
                                if (typeof c !== "number") {

                                } else {
                                    if (!this.state.firstCommitName && !!firstCommitName) {
                                        this.setState({
                                            commits: commits,
                                            firstCommitName: firstCommitName,
                                            isGettingCommits: false,
                                            totalCommits: c
                                        });
                                    } else {

                                        this.setState({
                                            commits: commits,
                                            isGettingCommits: false,
                                            totalCommits: c
                                        });
                                    }
                                }
                            })
                        } else {
                            if (!this.state.firstCommitName && !!firstCommitName) {
                                this.setState({
                                    commits: commits,
                                    firstCommitName: firstCommitName,
                                    isGettingCommits: false
                                });
                            } else {
                                this.setState({
                                    commits: commits,
                                    isGettingCommits: false
                                });

                            }
                        }
                    } else {
                        message.error("Failed load commits")
                    }

                })

            }
        }

    }

    getLoadingIndicator(isSmall, size) {
        if (!!isSmall && isSmall === true) {
            return <div className="VNoIconMargin" style={{
                height: '22px',
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                textAlign: 'center',
                justifyContent: 'center',
                color: '#1890ff'
            }}>
                <Icon type="loading" style={{fontSize: !!size ? size + "px" : 16 + "px"}} spin/>
            </div>
        } else {
            return <div>
                <h1>Loading...</h1>
                {this.getLoadingIndicator(true, 32)}
            </div>
        }
    }

    branches() {
        const th = this;
        if (this.state.branches === undefined) {
            return <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', width: '100%'}}>
                <Card style={{width: '80%', marginTop: '32px'}}>
                    {this.getLoadingIndicator()}
                </Card>
            </div>
        }

        if (this.state.branches.length > 0) {
            const list = [];
            for (const bId in this.state.branches) {
                const b = this.state.branches[bId];
                list.push(<List.Item>
                    <div style={{textAlign: "left"}} className="VFlex">
                        <span style={{fontSize: '18px'}} className="VFlex-Row">
                        {b}
                            {b === this.state.defaultBranch ?
                                <Icon type="star" style={{marginLeft: '8px', fontSize: '16px'}}/> : <span/>}
                        </span>

                        <span className="VFlex-Row">
                            {
                                b === this.state.defaultBranch ? <span/> :
                                    <Button type="dashed" style={{marginLeft: '16px'}} onClick={() => {
                                        const i = message.loading("Setting...", 0);
                                        const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
                                        API.setDefBranch(path, b, () => {
                                            i();
                                            message.loading("Updating...", 1);
                                            th.setState({
                                                branches: undefined,
                                                defaultBranch: undefined,
                                                lastUpdate: 0
                                            });
                                            th.update();
                                        })
                                    }}>
                                        Set Default
                                    </Button>
                            }
                            {
                                b === this.state.defaultBranch ? <span/> :
                                    <Button type="danger" style={{marginLeft: '16px'}} onClick={() => {
                                        Modal.confirm({
                                            title: "Confirmation",
                                            content: "Do you really want delete '" + b + "' branch?",
                                            okType: 'danger',
                                            onOk: () => {
                                                const i = message.loading("Removing...", 0);
                                                const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
                                                API.removeBranch(path, b, () => {
                                                    i();
                                                    message.loading("Updating...", 1);
                                                    th.setState({
                                                        branches: undefined,
                                                        defaultBranch: undefined,
                                                        lastUpdate: 0
                                                    });
                                                    th.update();
                                                })
                                            },
                                            autoFocusButton: 'cancel'
                                        })
                                    }}>
                                        Delete
                                    </Button>
                            }
                        </span>
                    </div>
                </List.Item>)
            }
            return <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', width: '100%'}}>
                <Card style={{width: '80%', marginTop: '32px'}} className="VFlex">
                    <div className="VFlex"
                         style={{display: 'flex', justifyContent: 'flex-end', flexDirection: 'column', width: '100%'}}>
                        <Button type="primary" style={{width: '200px'}} onClick={() => {
                            let t = "";
                            const i = <Input placeholder="Input here" onChange={(e) => {
                                t = e.target.value;
                            }}/>;
                            Modal.info({
                                title: <span style={{marginLeft: '6px'}}>Input branch name</span>,
                                content: (<div>
                                    {i}
                                </div>),
                                onOk: function () {
                                    const msg = message.loading("Creating new branch...", 0);
                                    const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
                                    API.newBranch(path, t, () => {
                                        msg();
                                        message.loading("Updating...", 2);
                                        th.setState({
                                            branches: undefined,
                                            defaultBranch: undefined,
                                            lastUpdate: 0
                                        });
                                        th.update();
                                    })
                                }
                            })
                        }}>
                            Create new
                        </Button>
                    </div>
                    <div className="VFlex-Row">
                        <List>
                            {list}
                        </List>
                    </div>
                </Card>
            </div>

        } else {

            return <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', width: '100%'}}>
                <Card style={{width: '80%', marginTop: '32px'}}>
                    Branches empty
                </Card>
            </div>
        }
    }

    file() {

    }

    tree() {
        let path = window.location.href.replace(window.location.origin, "").substr(12 + this.getRepoPath().length);
        const {fileViewData} = this.state;
        if (fileViewData.isLoading && fileViewData.path !== path){
            this.fileViewerGoto(path);
        }
        return this.makeFileViewer();

    }

    updateFiles(path) {

        if (!!this.state.repository) {

            if (!this.state.isGettingFiles) {

                this.setState({
                    currentPath: path,
                    isGettingFiles: true
                });

                API.getTree(this.state.repository.path, path, (d) => {
                    let files = this.state.files;
                    if (!files) {
                        files = {};
                    }
                    const obj = {};
                    for (const fff in d) {
                        const docum = d[fff];
                        obj[docum.name.toLowerCase()] = docum;
                    }
                    files[this.state.currentPath] = obj;
                    this.setState({
                        files: files,
                        isGettingFiles: false
                    });
                })
            }

        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        Prism.highlightAll();
        if (this.state.page === 'commits') {
            if (!this.state.commits) {
                this.updateCommits(20, 0)
            }
        }
    }

    componentDidMount() {
        Prism.highlightAll();
    }

    getCommitSkeleton() {
        return <Card style={{width: '80%', marginTop: '32px'}} className="ant-skeleton-content ">
            <div style={{display: 'flex', justifyContent: "space-between", alignItems: 'center'}}>
                <div style={{display: 'flex', justifyContent: "space-between", alignItems: 'center'}}>
                    <Avatar className="ant-skeleton-avatar" style={{backgroundColor: '#0277BD'}} size={32}/>

                    <span style={{marginLeft: '6px', height: '16px', width: '102px', marginTop: '0'}}
                          className="ant-skeleton-title "/>
                    <span style={{marginLeft: '6px', height: '14px', width: '402px', marginTop: '0'}}
                          className="ant-skeleton-title "/>
                </div>
                <span style={{
                    float: "right",
                    display: 'flex',
                    justifyContent: "space-between",
                    alignItems: 'center',
                    flexDirection: 'column'
                }}>

                        <span style={{marginLeft: '6px', height: '16px', width: '120px', marginTop: '0'}}
                              className="ant-skeleton-title "/>
                        <span style={{marginLeft: '6px', height: '16px', width: '60px', marginTop: '6px'}}
                              className="ant-skeleton-title "/>

                    </span>
            </div>
        </Card>

    }

    getTitleSkeleton() {
        return <Card style={{width: '80%', marginTop: '32px', display: 'block'}} className="ant-skeleton-content ">

            <h1 style={{
                marginBlockEnd: '0px',
                marginBlockStart: '0px',
                marginBottom: '0px',
                display: 'block',
                height: '32px'
            }} className="ant-skeleton-title"/>
            <h2 className="ant-skeleton-title " style={{

                marginBlockEnd: '0px',
                marginBlockStart: '0px',
                marginBottom: '0px',
                marginTop: '12px',
                height: '28px'
            }}/>
            <h2 className="ant-skeleton-title " style={{

                marginBlockEnd: '0px',
                marginBlockStart: '0px',
                marginBottom: '0px',
                marginTop: '12px',
                height: '16px'
            }}/>

        </Card>

    }

    getRepo(cb) {
        const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
        if (!!$stage.app.reposPaths[path]) {
            const r = $stage.app.reposPaths[path];

            this.setState({
                repository: r
            });
            cb();
        } else {
            API.loadRepoByPath(path, (ok, e) => {
                if (!ok) {
                    Modal.error({
                        title: "Failed load repository (" + e.code + ")",
                        content: e.message
                    })
                } else {
                    this.setState({
                        repository: e
                    });
                    cb();

                }
            })
        }
    }

    update() {
        if ((API.time() - this.state.lastUpdate) > 60) {
            this.setState({
                lastUpdate: API.time()
            });
            //alert("ITS TIME TO STOP!");
            if (!this.state.repository) {
                this.getRepo(() => {
                    if (this.state.page === 'main') {
                        this.updateFiles("/");
                    }
                    if (this.state.page !== 'commits') {
                        this.updateCommits(1, 0);
                    }
                    this.getBranches();
                })
            } else {
                if (this.state.page === 'main') {
                    this.updateFiles("/");
                }
                if (this.state.page !== 'commits') {
                    this.updateCommits(1, 0);
                }
                this.getBranches();

            }

        }

    }

    getRepoPath() {
        return API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5]
    }

    getDiff(o, n) {
        if (this.state.diffPath !== o + "/" + n) {
            if (!!this.state.repository) {
                this.setState({
                    diffPath: o + "/" + n
                });
                if (this.state.page === 'diffs') {
                    API.getDiffs(API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5], o, n, (ok, data) => {
                        if (!ok) {
                            if (this.state.page === 'diffs') {
                                this.setPage("main");
                                message.error("Failed load difference :(");
                            }
                        } else {
                            this.setState({
                                diffs: data
                            })
                        }
                    });
                }
            }
        }
    }

    diffs() {
        if (this.state.page == 'diffs') {
            const o = API.getParts(window.location.href)[7];
            const n = API.getParts(window.location.href)[8];
            this.getDiff(o, n);
            if (!!this.state.diffs) {

                const items = [];
                for (const dKey in this.state.diffs) {
                    const diff = this.state.diffs[dKey];
                    let lang = "";
                    if (diff.newPath.indexOf(".") !== -1) {
                        const l = diff.newPath.substr(diff.newPath.lastIndexOf(".") + 1);
                        if (!!$LANGS[l]) {
                            lang = $LANGS[l]
                        } else {
                            lang = l;
                        }
                    }
                    let bg = "#EEEEEE";
                    switch (diff.changeType) {
                        case ("MODIFY"):
                            bg = "#E3F2FD";
                            break;
                        case ("ADD"):
                            bg = "#E8F5E9";
                            break;
                        case ("COPY"):
                            bg = "#EDE7F6";
                            break;
                        case ("DELETE"):
                            bg = "#FBE9E7";
                            break;
                    }
                    items.push(
                        <CollapsePanel header={diff.newPath} key={dKey} style={{textAlign: 'left', background: bg}}>

                            <MonacoDiffEditor
                                height="500px"
                                language={lang}
                                original={diff.oldFile}
                                value={diff.newFile}
                                theme="vs-light"
                                options={{
                                    enableSplitViewResizing: false,
                                    renderSideBySide: false
                                }}
                            />


                        </CollapsePanel>
                    )
                }
                return <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    width: '100%'
                }}>

                    <Card style={{width: '80%', marginTop: '32px'}}>

                        <Collapse defaultActiveKey={['0']}>
                            {items}
                        </Collapse>

                    </Card>

                </div>
            } else {

                return <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    width: '100%'
                }}>
                    <Card style={{width: '80%', marginTop: '32px'}}>

                        <div style={{paddingTop: '24px', paddingBottom: '24px'}}>
                            {this.getLoadingIndicator()}
                        </div>

                    </Card>

                </div>
            }
        }
    }

    commits() {
        let peg = <span>Loading...</span>;
        let upd = <span/>;
        if (this.state.cUpdating) {
            upd = <span>Loading...</span>
        }
        if (!!this.state.totalCommits) {
            peg = <Pagination
                total={this.state.totalCommits}
                current={this.state.cPage}
                onChange={(e, b) => {
                    const offset = e * 20 - 20;
                    let count = this.state.totalCommits - offset;
                    if (count > 20) {
                        count = 20;
                    }
                    this.setState({
                        count: count,
                        offset: offset,
                        cPage: e,
                        cUpdating: true
                    })
                }}
                showTotal={(total, range) => {
                    return `${range[0]}-${range[1]} of ${total} items`;
                }}
                pageSize={20}
                defaultCurrent={1}
            />
        }
        const items = this.state.commitList;
        const addCommit = (comit) => {
            return <List.Item style={{textAlign: "left"}}>
                <div className="VFlex" style={{flexDirection: 'column', width: '100%'}}>
                    <div className="VFlex"
                         style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                        <div className="VFlex" style={{flexDirection: 'column'}}>
                            <div className="VFlex-Row VFlex" style={{flexDirection: 'row', alignItems: 'center'}}>

                                <Avatar className="VHeader-Content" size={32}
                                        src={"https://www.gravatar.com/avatar/" + md5(comit.author.email) + "?s=32"}/>
                                <span style={{marginLeft: '6px', fontWeight: 'bold', fontSize: '18px'}}>
                                                {comit.author.name}
                                                </span>
                            </div>
                            <div style={{float: 'right', paddingTop: '6px', width: '100%', minWidth: '200px'}}>
                                <pre style={{padding: '6px', borderRight: '6px', background: "#FAFAFA", width: '100%'}}>
                                   {comit.fullMessage}
                                </pre>
                            </div>
                        </div>
                        <div className="VFlex" style={{flexDirection: 'column', alignItems: 'flex-end'}}>

                            <Tooltip title={"At: " + moment(comit.time * 1000).format("HH:mm DD/MM/YYYY")}>
                                <div style={{fontWeight: 'bold'}}>
                                    {moment(comit.time * 1000).fromNow()}
                                </div>
                            </Tooltip>
                            {comit.name.substr(0, 10)}

                            <ButtonGroup>

                                <Tooltip title="Browse files">
                                    <Button size="small" icon="folder"/>
                                </Tooltip>
                                {/*
                                <Tooltip title="Download .zip">
                                    <Button size="small" icon="cloud-download"/>
                                </Tooltip>
*/}
                                {!!comit.parent ?
                                    <Tooltip title="Show difference">
                                        <Button size="small" icon="diff" onClick={() => {
                                            this.setPage("diffs/" + comit.parent + "/" + comit.name)
                                        }}/>
                                    </Tooltip> : <span/>}

                                <Tooltip title="Copy commit id">
                                    <Button size="small" icon="copy" onClick={() => {
                                        navigator.clipboard.writeText(comit.name)
                                            .then(() => {
                                                message.info("Success copied!", 0.75)
                                            })
                                            .catch(err => {
                                                message.error("Failed to copy :(", 2);
                                                console.log(err);
                                            });
                                    }}/>
                                </Tooltip>
                            </ButtonGroup>
                        </div>
                    </div>

                    <div className="VFlex" style={{flexDirection: 'row', width: '100%'}}>
                    </div>
                </div>
            </List.Item>
        };
        if (this.state.pos !== (this.state.count + "-" + this.state.offset)) {
            this.updateCommits(this.state.count, this.state.offset, (c) => {
                const items = [];
                for (const id in c) {
                    const ii = c[id];
                    items.push(addCommit(ii))
                }

                this.setState({
                    pos: this.state.count + "-" + this.state.offset,
                    commitList: items,
                    cUpdating: false
                })
            });
        }
        if (!!this.state.commits && !!this.state.totalCommits) {

            if (Object.keys(this.state.commits).length < 20) {
                if (this.state.getO !== this.state.offset) {
                    const of = this.state.offset;
                    this.updateCommits(this.state.count, of, (commits) => {
                        const items = [];
                        for (const commitId in commits) {
                            const commit = commits[commitId];
                            items.push(
                                addCommit(commit)
                            )
                        }

                        this.setState({
                            commitList: items,
                            getO: of
                        })
                    });
                }
            }

        }

        if (this.state.cUpdating) {

            return <div style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: '100%'
            }}>
                <Card style={{width: '80%', marginTop: '32px'}}>

                    {peg}
                    <div style={{paddingTop: '24px', paddingBottom: '24px'}}>
                        {this.getLoadingIndicator()}
                    </div>
                    {peg}

                </Card>

            </div>
        }
        if (items.length > 0) {
            return <div style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: '100%'
            }}>
                <Card style={{width: '80%', marginTop: '32px'}}>
                    {peg}
                    <List
                        size="large"
                    >
                        {items}
                    </List>

                    {peg}
                    {upd}

                </Card>

            </div>
        }
        if (this.state.totalCommits === 0) {

            return <div style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: '100%'
            }}>

                <Card style={{width: '80%', marginTop: '32px'}}>

                    <h1>Commits empty</h1>
                </Card>

            </div>
        }

        return <div style={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            width: '100%'
        }}>

            <Card style={{width: '80%', marginTop: '32px'}}>

                <h1>Loading...</h1>
                <Spin indicator={<Icon type="loading" style={{fontSize: 24}} spin/>}/>
            </Card>

        </div>

    }


    fileViewerGoto(path){
        const {fileViewData} = this.state;
        if (!fileViewData.isLoading) {
            this.setPage("files" + path);
            fileViewData.isP = true;
            fileViewData.path = path;
            fileViewData.files = undefined;
            fileViewData.show = ([
                <Card style={{width: '80%', marginTop: '30px'}}>
                    {this.getLoadingIndicator()}
                </Card>
            ]);
        }

    }
    makeFileViewer(data) {
        let fileViewData = data;
        if (!fileViewData) {
            fileViewData = this.state.fileViewData;
        }
        if (fileViewData.isLoading) {
            return <div style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: '100%'
            }}>
                {fileViewData.show}
            </div>;
        }
        if (!!fileViewData.files && !!this.state.repository) {
            const r = this.state.repository;
            const data = [];
            const breads = [];
            let mark = <span />;

            const f = fileViewData.path.split("/");
            let tPath = "";
            for (const pKey in f) {
                const p = f[pKey];
                tPath += p + "/";
                const onCPath = tPath;
                breads.push(
                    <Breadcrumb.Item onClick={() => {
                        this.fileViewerGoto(onCPath.substr(0, onCPath.length - 1));
                    }}  style={{cursor: "pointer"}}>{p}</Breadcrumb.Item>
                )
            }

            const files = {};
            for (const fKey in fileViewData.files) {
                const file = fileViewData.files[fKey];
                files[file.name.toLowerCase()] = file;
                data.push({
                    clc: () => {
                      if (file.isDirectory){
                          this.fileViewerGoto(file.path)
                      } else {

                      }
                    },
                    name: (
                        <div style={{cursor: "pointer"}}>
                            <Icon type={file.isDirectory ? "folder" : "file"} style={{marginRight: '6px'}}/>
                            {file.name}
                        </div>
                    ),
                    message: file.commit.message,
                    date: moment(file.commit.time * 1000).fromNow()
                })
            }

            if (!!files['readme.md']) {
                API.getFileContent(r.path, files['readme.md'].path, (res, bool, xhr) => {
                    if (bool) {
                        const i = new markdownIt({
                            highlight: function (str, lang) {
                                return '<pre class="language-' + lang + '"><code>' + str + '</code></pre>';
                            }
                        });
                        mark = (
                            <Card style={{width: '80%', marginTop: '32px', textAlign: 'left'}}
                                  title={files['readme.md'].name}>
                                {ReactHtmlParser(i.render(res))}
                            </Card>
                        );
                    }
                });
            }
            return (
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    width: '100%'
                }}>
                    <Card style={{width: '80%', marginTop: '30px'}}>
                        <Breadcrumb style={{textAlign: "left"}}>
                            <Breadcrumb.Item onClick={() => {
                                this.fileViewerGoto("/");
                            }} style={{cursor: "pointer"}}>
                                {r.name}
                            </Breadcrumb.Item>
                            {breads}
                        </Breadcrumb>
                        <Table style={{marginTop: '6px'}} dataSource={data} columns={[
                            {
                                dataIndex: "name",
                                key: "name",
                                render: (f) => {
                                    return <div style={{textAlign: "left"}}>{f}</div>
                                }
                            },
                            {
                                dataIndex: "message",
                                key: "message",
                                render: (f) => {
                                    return <div style={{textAlign: "right"}}>{f}</div>
                                }
                            },
                            {
                                dataIndex: "date",
                                key: "date",
                                render: (f) => {
                                    return <div style={{textAlign: "right"}}>{f}</div>
                                }
                            },
                        ]} showHeader={false} pagination={false} onRow={(record, rowIndex) => {
                            return {
                                onClick: event => {data[rowIndex].clc()}, // click row
                                onDoubleClick: event => {}, // double click row
                                onContextMenu: event => {}, // right button click row
                                onMouseEnter: event => {}, // mouse enter row
                                onMouseLeave: event => {}, // mouse leave row
                            };
                        }}/>
                    </Card>
                    {mark}
                </div>


            )
        } else {
            console.log(fileViewData);
            if (!!fileViewData.path) {
                const {path} = fileViewData;

                fileViewData.isLoading = true;
                fileViewData.files = undefined;
                this.setState({
                    fileViewData: fileViewData
                });

                API.getTree(this.getRepoPath(), path, (d) => {
                    if (!!d.error) {

                        fileViewData.view = [];
                        fileViewData.view.push(
                            <Card style={{width: '80%', marginTop: '30px'}}>
                                Failed load tree
                            </Card>
                        );
                        this.setState({
                            fileViewData: fileViewData
                        })
                    } else {
                        fileViewData.files = d;
                        fileViewData.isLoading = false;
                        this.setState({
                            fileViewData: fileViewData
                        })
                    }
                });

                return (
                    <Card style={{width: '80%', marginTop: '30px'}}>
                        {this.getLoadingIndicator()}
                    </Card>
                )
            } else {
                fileViewData.path = "/";
                this.setState({
                    fileViewData: fileViewData
                })
            }
        }


        return <div style={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            width: '100%'
        }}>
            {fileViewData.show}
        </div>;
    }

    main() {
        this.update();
        const cards = [];
        let d = (<div className="ant-skeleton ant-skeleton-active" style={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column'
        }}>

            {this.getTitleSkeleton()}
            {this.getCommitSkeleton()}
            {this.getFilesSkeleton()}

        </div>);
        let r = this.state.repository;

        const list = [];

        if (!!r && !this.state.commits) {
            this.updateCommits(1, 0);
        }

        if (!!r) {

            if (!!this.state.commits && !!this.state.commits[this.state.firstCommitName]) {
                cards.push(
                    <Card style={{width: '80%', marginTop: '32px'}}>
                        <div style={{display: 'flex', justifyContent: "space-between", alignItems: 'center'}}>
                            <div style={{display: 'flex', justifyContent: "space-between", alignItems: 'center'}}>
                                <Avatar className="VHeader-Content" size={32}
                                        src={"https://www.gravatar.com/avatar/" + md5(this.state.commits[this.state.firstCommitName].author.email) + "?s=32"}/>
                                <span style={{marginLeft: '6px', fontWeight: 'bold'}}>
                                        {this.state.commits[this.state.firstCommitName].author.name}
                                        </span>
                                <span style={{marginLeft: '6px'}}>
                                        {this.state.commits[this.state.firstCommitName].message}
                                        </span>
                            </div>
                            <span style={{float: "right"}}>
                                    <div style={{fontWeight: 'bold'}}>
                                        {moment(this.state.commits[this.state.firstCommitName].time * 1000).fromNow()}
                                    </div>
                                {this.state.commits[this.state.firstCommitName].name.substr(0, 10)}
                                </span>
                        </div>
                    </Card>
                );
            } else {
                if (!!r) {

                    cards.push(
                        this.getCommitSkeleton()
                    );
                }
            }

            if (this.state.fileViewData.hasFile === false) {
                const url = API.baseUrl + "repo/" + r.path + ".git";
                const name = r.name;
                const newProject =
                    "git clone " + url + " \"" + name + "\"\n" +
                    "cd \"" + r.name + "\"\n" +
                    "touch README.md\n" +
                    "git add README.md\n" +
                    "git commit -m \"add README\"\n" +
                    "git push -u origin master";
                const alreadyCreated =
                    "cd existing_folder\n" +
                    "git init\n" +
                    "git remote add origin " + url + "\n" +
                    "git add .\n" +
                    "git commit -m \"Initial commit\"\n" +
                    "git push -u origin master";

                const withGit =
                    "cd existing_repo\n" +
                    "git remote rename origin old-origin\n" +
                    "git remote add origin " + url + "\n" +
                    "git push -u origin --all\n" +
                    "git push -u origin --tags";

                cards.push(
                    <Card style={{width: '80%', marginTop: '32px', textAlign: 'left'}}>
                        <h2>Repository successfully created!</h2>
                        <h3>Upload your project via Git:</h3>
                        <h4>Push an existing folder:</h4>
                        <pre className="language-bash"><code>{alreadyCreated}</code></pre>
                        <h4>Create a new repository:</h4>
                        <pre className="language-bash"><code>{newProject}</code></pre>
                        <h4>Push an existing Git repository:</h4>
                        <pre className="language-bash"><code>{withGit}</code></pre>
                    </Card>
                );
            } else {
                cards.push(this.makeFileViewer({
                    path: "/",
                    files: undefined,

                }))
            }

            d = (
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    width: '100%'
                }}>
                    <Card style={{width: '80%', marginTop: '32px', textAlign: 'left'}}>

                        <h2 style={{
                            marginBlockEnd: '0px',
                            marginBlockStart: '0px',
                            marginBottom: '0px'
                        }}>{r.name}</h2>
                        <h3 style={{

                            marginBlockEnd: '0px',
                            marginBlockStart: '0px',
                            marginBottom: '0px'
                        }}>{r.description}</h3>

                        <div style={{float: 'right'}}>

                            <Popover placement="right" style={{minWidth: '412px'}} content={
                                <div style={{minWidth: '412px'}}>
                                    <Input addonAfter={<Icon type="copy" onClick={() => {
                                        navigator.clipboard.writeText(API.baseUrl + "repo/" + r.path + ".git")
                                            .then(() => {
                                                message.info("Success copied!", 0.75)
                                            })
                                            .catch(err => {
                                                message.error("Failed to copy :(", 2);
                                                console.log(err);
                                            });
                                    }}/>} style={{marginTop: '6px', textAlign: 'center'}}
                                           value={API.baseUrl + "repo/" + r.path + ".git"}/>
                                </div>} title="Clone VGit repository" trigger="click">

                                <Button type="primary">Clone</Button>
                            </Popover>
                        </div>
                    </Card>

                    {cards}
                </div>
            )
        }

        return <div className="VFlex VPage-Full ant-skeleton-content ant-skeleton ant-skeleton-active"
                    style={{alignItems: 'center'}}>
            {d}

        </div>
    }

    setPage(page, cb) {
        const path = API.getParts(window.location.href)[4] + "/" + API.getParts(window.location.href)[5];
        API.setPage("repo/" + path + "/" + page);
        this.forceUpdate(cb)

    }

    render() {
        this.update();
        const parts = API.getParts(window.location.href);
        const partsLenght = API.getParts(window.location.href).length;

        if (partsLenght > 6) {
            if (parts[6] !== this.state.page) {
                this.setState({
                    page: parts[6]
                })
            }
        } else {
            if (this.state.page === undefined) {
                this.setState({
                    page: "main"
                })
            }
        }

        var p = undefined;

        if (!!this.pages[this.state.page]) {
            p = this.pages[this.state.page]();
        }

        if (!p) {

            p = <div><h1>Request panel not found</h1></div>
        }
        return (

            <Layout style={{minHeight: "100%"}}>

                <Layout.Sider width='260' theme="light" style={{background: "#01386f", textAlign: 'left'}}>

                    <Menu
                        mode="inline"
                        defaultSelectedKeys={['1']}
                        defaultOpenKeys={['sub1']}
                        style={{height: '100%', borderRight: 0}}
                    >
                        <Menu.Item key="1" onClick={() => {
                            this.setPage("main")
                        }}>Details</Menu.Item>
                        <Menu.Item key="2" onClick={() => {
                            this.setPage("issues")
                        }}>Issues</Menu.Item>
                        <Menu.Item key="3" onClick={() => {
                            this.setPage("branches")
                        }}>
                            <div className="VFlex VFlex-Row" style={{
                                display: 'flex',
                                justifyContent: "space-between",
                                alignItems: 'center'
                            }}>Branches <Tag color="blue" style={{float: 'right'}}>
                                {(!!this.state.branches) ? this.state.branches.length : this.getLoadingIndicator(true)}
                            </Tag></div>
                        </Menu.Item>
                        <Menu.Item key="4" onClick={() => {

                            this.setPage("commits")
                        }}>
                            <div className="VFlex VFlex-Row" style={{
                                display: 'flex',
                                justifyContent: "space-between",
                                alignItems: 'center'
                            }}>Commits <Tag color="blue" style={{float: 'right'}}>
                                {(typeof this.state.totalCommits === "number") ? this.state.totalCommits : this.getLoadingIndicator(true)}
                            </Tag></div>
                        </Menu.Item>
                        <SubMenu
                            key="sub1"

                            title={
                                <span>
                                   <Icon type="user"/>
                                   CI / CD
                                </span>
                            }
                        >
                            <Menu.Item key="5">LinePipes</Menu.Item>
                            <Menu.Item key="6">Runners</Menu.Item>
                            <Menu.Item key="7">CD</Menu.Item>
                        </SubMenu>
                    </Menu>

                </Layout.Sider>
                <Layout.Content style={{minHeight: "100%"}}>
                    <div id="subContent">
                        {p}
                    </div>
                </Layout.Content>

            </Layout>
        );
    }

    getFilesSkeleton() {

        return <Card style={{width: '80%', marginTop: '32px', display: 'block'}} className="ant-skeleton-content ">


            <Breadcrumb separator="/">
                <Breadcrumb.Item>
                    <span style={{height: '16px', width: '60px'}} className="ant-skeleton-title "/>
                </Breadcrumb.Item>
            </Breadcrumb>

            <List
                className="ant-skeleton-content"
                size="small"
                style={{width: '100%', display: "block"}}
            >
                <List.Item style={{textAlign: "left"}}>

                    <span style={{height: '16px', width: '16px'}} className="ant-skeleton-title "/>
                    <span style={{marginLeft: '12px', height: '18px', width: '365px'}} className="ant-skeleton-title "/>

                </List.Item>
                <List.Item style={{textAlign: "left"}}>

                    <span style={{height: '16px', width: '16px'}} className="ant-skeleton-title "/>
                    <span style={{marginLeft: '12px', height: '18px', width: '165px'}} className="ant-skeleton-title "/>

                </List.Item>
                <List.Item style={{textAlign: "left"}}>

                    <span style={{height: '16px', width: '16px'}} className="ant-skeleton-title "/>
                    <span style={{marginLeft: '12px', height: '18px', width: '265px'}} className="ant-skeleton-title "/>

                </List.Item>
            </List>
        </Card>

    }
}