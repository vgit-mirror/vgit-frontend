import React from "react";
import Icon from "antd/es/icon";
import {Spin} from "antd";

export class VRepoElement extends React.Component {


    repo;
    repository;
    lastCommit;
    loader = <Spin indicator={<Icon className type="loading" style={{fontSize: 24}} spin/>}/>;
    static loader = <Spin indicator={<Icon className type="loading" style={{fontSize: 24}} spin/>}/>;

    constructor(props) {
        super(props);
        this.repo = props.repo;
        this.afterInit();

    }

    static ifNonGetLoader(object, non, loader) {
        if (object !== undefined) {
            return non(object);
        } else {
            return !!loader ? loader : VRepoElement.loader;
        }
    }

    ifNonGetLoader(object, non, loader) {
        return VRepoElement.ifNonGetLoader(object, non, loader ? loader : this.loader);
    }

    reload(cb) {
        this.repo.eventEmitter.once("updated", cb);
        this.repo.eventEmitter.emit("reload");
    }

    Repo() {
        return this.repo.repo;
    }

    afterInit() {
    }

}