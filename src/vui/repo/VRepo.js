import React from "react";
import {VRepoHeader, VRepoSiderHeader} from "./elements/VRepoHeader";
import EventEmitter from 'event-emitter';
import API, {TOOLZ} from "../../api/api";
import {Card, message} from "antd";
import {Modal} from "antd/es";
import {VRepoMain} from "./pages/VRepoMain";
import Menu from "antd/es/menu";
import Icon from "antd/es/icon";
import SubMenu from "antd/es/menu/SubMenu";
import {VRepoArtifacts} from "./pages/VRepoArtifacts";
import {VRepoIssues} from "./pages/VRepoIssues";
import {VRepoCommits} from "./pages/VRepoCommits";
import {VRepoBranches} from "./pages/VRepoBranches";
import {VRepoCD} from "./pages/VRepoCD";
import {VRepoCI} from "./pages/VRepoCI";
import {VRepoBS} from "./pages/VRepoBS";
import {VRepoSettings} from "./pages/VRepoSettings";
import {VRepoWelcome} from "./pages/VRepoWelcome";
import VRepoDiffs from "./pages/VRepoDiffs";
import './VRepo.scss'
import Badge from "antd/es/badge";
import {VRepoElement} from "./VRepoElement";
import {VRepoSider} from "./elements/VRepoSider";

export class VRepo extends React.Component {

    state = {
        sider: TOOLZ.isWidthAdaptive(true, false)
    };
    contentRenderer = {
        main: () => <VRepoMain repo={this}/>,
        artifacts: () => <VRepoArtifacts repo={this}/>,
        issues: () => <VRepoIssues repo={this}/>,
        commits: () => <VRepoCommits repo={this}/>,
        branches: () => <VRepoBranches repo={this}/>,
        cd: () => <VRepoCD repo={this}/>,
        ci: () => <VRepoCI repo={this}/>,
        bs: () => <VRepoBS repo={this}/>,
        settings: () => <VRepoSettings repo={this}/>,
        welcome: () => <VRepoWelcome repo={this}/>,
        diffs: () => (<VRepoDiffs repo={this}/>)
    };
    // TITLE
    title = "Repository";
    // Repository page [Main - default]
    page = undefined;
    //
    pageData = [];
    // Repository entity
    repo = undefined;
    // Repository path
    path = undefined;
    // Last commit in repo
    lastCommit = undefined;
    // Event manager
    eventEmitter = new EventEmitter();
    // CHANGED PAGE
    pageChanged = true;
    // STATUS (LOADING)
    status = {
        isLoadingCommitCount: false,
        isLoadingIssuesCount: false,
        isLoadingBranch: false,
        isLoadingRepo: false,
        isLoadingLastCommit: false
    };
    // ALL COMMIT COUNT
    totalCommits = undefined;
    // ALL ISSUES COUNT
    totalIssues = undefined;
    // BRANCHES
    branches = undefined;
    // DEFAULT BRANCH
    defaultBranch = undefined;


    constructor(props) {
        super(props);
        const parts = API.getParts(window.location.href);
        if (parts.length < 6 || parts[5] === "" || parts[4] === "") {
            this.exit("Repository not found", "Invalid mapping!");
            return;
        }

        if (!!parts[6]) {
            if (parts[6] !== "")
                this.page = parts[6];
            else
                this.page = "main"
        }

        this.path = parts[4] + "/" + parts[5];
        if (!!parts[6]) {
            this.page = parts[6];
        }
        this.eventEmitter.on("reload", () => {
            this.forceUpdate(() => this.eventEmitter.emit("updated"));
        });
    }

    WIP() {
        return (
            <div>

                <h2>Requested page is WIP</h2>

            </div>
        )
    }


    getCommitCount() {
        if (!!this.repo) {
            if (!this.status.isLoadingCommitCount) {
                this.eventEmitter.emit("updating", {type: 'commitsCount'});
                this.status.isLoadingCommitCount = true;
                API.getCommitsCount(this.path, (ok, d) => {
                    this.eventEmitter.emit("updated_commits_count", d);
                    this.status.isLoadingCommitCount = false;
                    if (!!ok) {
                        this.totalCommits = d.count;
                        this.forceUpdate()
                    }
                })
            }
        }
    }


    getIssuesCount() {
        if (!!this.repo) {
            if (!this.status.isLoadingIssuesCount) {
                this.eventEmitter.emit("updating", {type: 'issuesCount'});
                this.status.isLoadingIssuesCount = true;
                API.getIssuesCount(this.path, (ok, d) => {
                    this.eventEmitter.emit("updated_issues_count", d);
                    this.status.isLoadingIssuesCount = false;
                    if (!!ok) {
                        this.totalIssues = d.count;
                        this.forceUpdate()
                    }
                })
            }
        }
    }

    getBranches() {
        if (!!this.repo) {
            if (!this.status.isLoadingBranch) {
                this.eventEmitter.emit("updating", {type: 'branches'});
                this.status.isLoadingBranch = true;
                API.getDefaultBranch(this.path, (ok, db) => {
                    this.status.isLoadingBranch = false;
                    if (!!ok) {

                        API.getBranches(this.path, (ok, data) => {
                            this.status.isLoadingBranch = false;
                            if (!ok) {
                                message.error("Failed load branches")
                            } else {
                                this.branches = data;
                                this.defaultBranch = db.branch;
                                this.forceUpdate()
                            }
                        })
                    } else {
                        message.error("Failed load default branch")
                    }
                })
            }
        }
    }

    changePage(goto) {
        this.page = goto.split("/")[0];
        const data = goto.split("/");
        data.splice(0, 1);
        this.pageData = data;
        window.history.pushState(this.page, this.title, '/repo/' + this.path + "/" + goto);
        this.pageChanged = true;
        this.forceUpdate();
    }

    exit(reason, description) {
        Modal.error({title: reason, content: description});
        API.setPage("main");
    }

    updateLastCommit() {
        if (!!this.repo) {
            if (!this.status.isLoadingLastCommit) {
                this.eventEmitter.emit("updating", {type: 'lastCommit'});
                this.status.isLoadingLastCommit = true;
                API.getCommits(this.path, 1, 0, (ok, commit) => {
                    this.status.isLoadingLastCommit = false;
                    if (ok) {
                        this.eventEmitter.emit("updated_commits_last", commit);
                        if (commit.length > 0) {
                            this.lastCommit = commit[0];
                            this.forceUpdate()
                        } else {
                            this.lastCommit = 0;
                        }
                    } else {
                        message.error("Failed load last commit :(")
                    }
                })
            }
        }
    }

    updateRepository() {
        if (!this.status.isLoadingRepo) {
            this.eventEmitter.emit("updating", {type: 'repository'});
            this.status.isLoadingRepo = true;
            API.getRepoByPath(this.path, (ok, repo) => {
                this.status.isLoadingRepo = false;
                if (!ok) {
                    this.exit("Fail to get repository", repo.message)
                } else {
                    this.repo = repo;
                    this.eventEmitter.emit("updated", {type: 'repository', 'repo': repo});
                    this.eventEmitter.emit("updated_repo", repo);
                    this.forceUpdate();
                }
            })
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        TOOLZ.setTitle(this.title);
        if (this.pageChanged) {
            this.pageChanged = false;
            this.eventEmitter.emit("page_change", this.page, this.pageData);
            this.eventEmitter.emit("page_change_" + this.page, this.page, this.pageData)
        }
    }

    update() {
        if (!!this.repo) {
            this.getCommitCount();
            this.getIssuesCount();
            this.getBranches();
            this.updateLastCommit();
            this.updateRepository();
        } else {
            this.eventEmitter.once("updated_repo", (repo) => {
                this.getCommitCount();
                this.getBranches();
                this.getIssuesCount();
                this.updateLastCommit();
            });
            this.updateRepository()
        }
    }

    componentWillUnmount() {

    }

    componentDidMount() {
        this.update()
    }

    render() {

        if (!this.page)
            this.page = "main";
        let content;
        if (!!this.contentRenderer[this.page]) {
            content = this.contentRenderer[this.page]();
        }

        if (!content) {
            content = (<Card><h2>Requested page '{this.page}' not found</h2></Card>)
        }


        return (
            <div className="VRepo-Main VFlex VFlex-Row">
                <div className={"VRepo-Background-Sider " + (this.state.sider ? "hide" : "show")} onClick={() => {
                    this.setState({
                        sider: true
                    })
                }}/>
                <VRepoSider repo={this} collapsed={this.state.sider}>
                    <VRepoSiderHeader repo={this}/>

                    <Menu
                        style={{width: "100%", textAlign: 'left'}} mode="inline"
                        selectedKeys={[this.page]}
                    >
                        <Menu.Item key="main" onClick={() => {
                            this.changePage('main')
                        }}>
                            <Icon type="info-circle"/>
                            General
                        </Menu.Item>
                        <Menu.Item key="artifacts" onClick={() => {
                            this.changePage('artifacts')
                        }}>
                            <Icon type="download"/>
                            Artifacts

                        </Menu.Item>
                        <Menu.Item key="issues" onClick={() => {
                            this.changePage('issues')
                        }}>

                            <div className="VFlex VFlex-Row VFlex-Center-V" style={{justifyContent: 'space-between'}}>
                                <div className="VFlex VFlex-Row VFlex-Center-V">
                                    <Icon type="close-circle"/>
                                    Issues

                                </div>
                                {VRepoElement.ifNonGetLoader(this.totalIssues, (f) => {

                                    if (f === 0) {
                                        return (
                                            <Badge
                                                count={0}
                                                style={{
                                                    backgroundColor: 'transparent',
                                                    color: '#999',
                                                    boxShadow: '0 0 0 1px #1976D2 inset'
                                                }}
                                                showZero
                                            />
                                        )
                                    } else {
                                        return (
                                            <Badge
                                                count={f}
                                                style={{
                                                    backgroundColor: 'transparent',
                                                    color: '#999',
                                                    boxShadow: '0 0 0 1px #E64A19 inset'
                                                }}
                                            />
                                        )
                                    }
                                })}
                            </div>
                        </Menu.Item>
                        <Menu.Item key="commits" onClick={() => {
                            this.changePage('commits')
                        }}>


                            <div className="VFlex VFlex-Row VFlex-Center-V" style={{justifyContent: 'space-between'}}>
                                <div className="VFlex VFlex-Row VFlex-Center-V">

                                    <Icon type="diff"/>
                                    Commits
                                </div>
                                {VRepoElement.ifNonGetLoader(this.totalCommits, (f) => {

                                    return (
                                        <Badge
                                            count={f}
                                            style={{
                                                backgroundColor: 'transparent',
                                                color: '#999',
                                                boxShadow: '0 0 0 1px #1976D2 inset'
                                            }}
                                            showZero
                                        />
                                    )

                                })}
                            </div>
                        </Menu.Item>
                        <Menu.Item key="branches" onClick={() => {
                            this.changePage('branches')
                        }}>
                            <Icon type="branches"/>
                            Branches
                        </Menu.Item>
                        <SubMenu
                            key="sub1"
                            title={

                                <span>
                                    <Icon type="cloud-server"/>
                                    <span>CI/CD</span>
                                </span>
                            }
                        >
                            <Menu.Item key="bs" onClick={() => {
                                this.changePage('bs')
                            }}>
                                <Icon type="cloud-sync"/>
                                Build Tasks
                            </Menu.Item>
                            <Menu.Item key="ci" onClick={() => {
                                this.changePage('ci')
                            }}>
                                <Icon type="check-circle"/>
                                CI
                            </Menu.Item>
                            <Menu.Item key="cd" onClick={() => {
                                this.changePage('cd')
                            }}>
                                <Icon type="cloud-download"/>
                                CD
                            </Menu.Item>
                        </SubMenu>
                        <Menu.Item key="settings" onClick={() => {
                            this.changePage('settings')
                        }}>
                            <Icon type="setting"/>
                            Settings
                        </Menu.Item>
                    </Menu>
                </VRepoSider>
                <div className="VRepo-Content">
                    <div className="trans"/>
                    <VRepoHeader repo={this}/>
                    <div className={"VRepo-Content-Inner"}>
                        {content}
                    </div>
                </div>

            </div>


        );
    }
}