export default {
    'dark': {
        variables: {
            diffViewerBackground: '#424242',
            addedBackground: '#2E7D3280',
            addedColor: '#24292e',
            removedBackground: 'rgba(211,47,47,0.26)',
            removedColor: '#24292e',
            wordAddedBackground: 'rgba(172,242,189,0.21)',
            wordRemovedBackground: 'rgba(253,184,192,0.2)',
            addedGutterBackground: '#2E7D32',
            removedGutterBackground: '#d32f2f80',
            gutterBackground: '#424242',
            gutterBackgroundDark: '#212121',
            highlightBackground: '#fffbdd',
            highlightGutterBackground: '#fff5b1',
            codeFoldGutterBackground: '#424242',
            codeFoldBackground: '#424242',
            emptyLineBackground: '#21212180',
        },

        diffContainer: {}, //style object
        diffRemoved: {}, //style object
        diffAdded: {}, //style object
        marker: {
            'pre': {
                color: "white !important"
            }
        }, //style object
        highlightedLine: {}, //style object
        highlightedGutter: {}, //style object
        gutter: {}, //style object
        line: {}, //style object
        wordDiff: {}, //style object
        wordAdded: {}, //style object
        wordRemoved: {}, //style object
        codeFoldGutter: {}, //style object
        emptyLine: {}, //style object
    },
    'light': {
        variables: {
            diffViewerBackground: '#fff',
            addedBackground: '#e6ffed',
            addedColor: '#24292e',
            removedBackground: '#ffeef0',
            removedColor: '#24292e',
            wordAddedBackground: '#acf2bd',
            wordRemovedBackground: '#fdb8c0',
            addedGutterBackground: '#cdffd8',
            removedGutterBackground: '#ffdce0',
            gutterBackground: '#f7f7f7',
            gutterBackgroundDark: '#f3f1f1',
            highlightBackground: '#fffbdd',
            highlightGutterBackground: '#fff5b1',
            codeFoldGutterBackground: '#dbedff',
            codeFoldBackground: '#f1f8ff',
            emptyLineBackground: '#fafbfc',
        }
    }
}