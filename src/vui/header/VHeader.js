import React from "react";
import './header.scss'
import {$stage} from "../../index";
import {Avatar} from "antd";
import md5 from "md5";
import Popover from "antd/es/popover";
import {Menu} from "antd/es";
import Icon from "antd/es/icon";
import Affix from "antd/es/affix";
import API from "../../api/api";
import {VGitUtils} from "../../utils/VGitUtils";


export class VHeader extends React.Component {
    state = {
        onTop: false
    };
    render() {
        const l = [];
        const r = [];
        const m = [];
        const newContent = <Menu style={{borderRight: '0'}}>
            <Menu.Item onClick={() => {
                API.setPage("repository-new")
            }}>Repository</Menu.Item>
            <Menu.Item>Group</Menu.Item>
        </Menu>;

        l.push(<div className="VHeader-Content VHeader-Icon trans" key={l.length}><Icon type="search"/></div>);

        const cm =
            <Menu style={{borderRight: '0'}}>
                {m}
            </Menu>;
        if ($stage.app.account) {
            m.push(
                <Menu.Item onClick={() => {
                    API.setPage("main")
                }} key={VGitUtils.getID()}><Icon type="smile"/> Profile</Menu.Item>
            );
            m.push(
                <Menu.Item onClick={() => {
                    $stage.app.token = undefined;
                    API.setPage("main")
                }} key={VGitUtils.getID()}><Icon type="lock"/> Logout</Menu.Item>
            );
            r.push(
                <Popover key={VGitUtils.getID()} content={newContent} title="Create new..." trigger="click"
                         placement="bottomRight">
                    <div className="VHeader-Content VHeader-Icon trans"><Icon type="plus"/></div>
                </Popover>);
            r.push(
                <Popover key={VGitUtils.getID()} placement="bottomRight" content={cm} title={$stage.app.account.login}>
                    <Avatar className="VHeader-Content" size={32}
                            src={"https://www.gravatar.com/avatar/" + md5($stage.app.account.email) + "?s=32"}/>
                </Popover>
            )
        } else {

            m.push(
                <Menu.Item key={VGitUtils.getID()} onClick={() => {
                    API.setPage("main")
                }}><Icon type="smile"/> Authorization</Menu.Item>
            );
        }
        return <Affix offsetTop={0} onChange={(f) => {
            this.setState({
                onTop: f
            })
        }}>
            <div className={"VHeader " + (!this.state.onTop ? "VHeader-OnTop" : "")}>
                <h1 className="VLogo trans" onClick={() => {
                    API.setPage("main")
                }}>
                    VGit
                </h1>
                {l}
                <div className="VHeader-Right" style={{
                    position: 'absolute',
                    'height': '48px',
                    'right': '6px',
                    'display': 'flex',
                    'alignItems': 'center'
                }}>
                    {r}
                </div>
            </div>
        </Affix>
    }
}