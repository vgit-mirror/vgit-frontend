import Icon from "antd/es/icon";
import React from "react";

import SVG from "./SVG";

export const VenityGitIcon = props => {
    return <Icon component={(f) => <SVG url="/VGIT.svg"/>} {...props}/>
};

