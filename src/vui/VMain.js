import {VAuth} from "./auth/VAuth";
import {$stage} from "../index";
import React from "react";
import {VProfile} from "./profile/VProfile";

export function VMain() {
    if (!$stage.app.token){
        return <VAuth/>
    } else {
        return <VProfile/>
    }
}