import * as React from "react";
import {Card, Spin} from "antd";
import {$stage} from "../../index";
import {VGitRepository} from "../../api/VGitPrototypes";
import Icon from "antd/es/icon";
import API from "../../api/api";
import {VGitUtils} from "../../utils/VGitUtils";

export class VRepositoriesCard extends React.Component {
    render() {

        if (!$stage.app.repos) {
            return (
                <div>
                    <Spin/>
                </div>
            )
        }
        const cards = [];

        for (const repoId in $stage.app.repos) {
            const repo: VGitRepository = $stage.app.repos[repoId];
            if (repo.owners.includes($stage.app.account.id)) {
                cards.push(
                    <Card size={"small"} key={VGitUtils.getID()} className="VProfile-Repo-Card VFlex" onClick={() => {
                        API.setPage("repo/" + repo.path)
                    }}>
                        <div className="VFlex VFlex-Row VFlex-Center-V">
                            <h2>{repo.name}</h2>
                            {repo.confidential ? <Icon type="lock"/> : <span/>}
                        </div>
                        <div className="VFlex VFlex-Row VFlex-Center-V">
                            {repo.description ? <h3>{repo.description}</h3> : <span/>}
                        </div>

                    </Card>
                )
            }
        }
        return (
            <div>
                {cards}
            </div>
        );
    }
}