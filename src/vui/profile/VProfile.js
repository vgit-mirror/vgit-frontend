import './profile.scss'
import * as React from "react";
import {$stage} from "../../index";
import Card from "antd/es/card";
import moment from "moment";
import Gravatar from "../../utils/Gravatar";
import CalendarHeatmap from 'react-calendar-heatmap';
import {VRepositoriesCard} from "./VRepositoriesCard.js";
import ReactTooltip from 'react-tooltip';
import {TOOLZ} from "../../api/api";

/* eslint-disable */
export class VProfile extends React.Component {

    state = {
        seeMoreCalendar: false
    };


    componentDidMount() {
    }

    render() {
        if ($stage.app.account === undefined) {
            return (
                <div>
                    Critical error
                </div>
            )
        }
        return (
            <div className="VFlex VFlex-Center VProfile">
                <Card className="VProfile-Info-Card">
                    <div className="VFlex VFlex-Center">
                        <div className="VFlex VFlex-Row  VFlex-Center">
                            <div className="VFlex VFlex-Row VFlex-Center-V trans">
                                <Gravatar email={$stage.app.account.email} imgSize={124}
                                          className="VProfile-Info-Avatar trans"/>
                                <div style={{margin: "16px", textAlign: "left"}} className="trans">
                                    <h2 className="trans">{$stage.app.account.login}</h2>
                                    <h3 className="trans">{$stage.app.account.fullName}</h3>
                                    <h3 className="trans">{$stage.app.account.status}</h3>
                                    <h3 className="trans">{moment($stage.app.account.registration * 1000).fromNow()}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </Card>
                <Card className="VProfile-Chart-Card" size={"small"}>
                    <CalendarHeatmap
                        startDate={TOOLZ.isWidthAdaptive(this.state.seeMoreCalendar ? new Date('2018-12-31') : new Date('2019-06-31'), new Date('2018-12-31'))}
                        endDate={new Date('2020-01-01')}
                        gutterSize={2}
                        values={[

                            {date: '2019-01-01', count: 6},
                            {date: '2019-01-22', count: 1},
                            {date: '2019-02-22', count: 2},
                            {date: '2019-03-22', count: 3},
                            {date: '2019-01-30', count: 12},

                        ]}
                        tooltipDataAttrs={value => {
                            if (!!value.date) {
                                return {
                                    'data-tip': `Commits at ${value.date}: ${value.count !== undefined ? value.count : 1}`,
                                };
                            } else {

                                return {
                                    'data-tip': `No commits`,
                                };
                            }
                        }}

                        classForValue={(value) => {
                            if (!value) {
                                return 'color-empty';
                            }
                            return `color-scale-${value.count !== undefined ? (value.count > 5 ? 5 : value.count) : 1}`;
                        }}
                    />
                    <ReactTooltip/>

                </Card>
                <Card className="VProfile-Repos-Card" size={"small"}>
                    <VRepositoriesCard/>
                </Card>
            </div>
        )
    }

}