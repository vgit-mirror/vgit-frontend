import * as React from "react";
import {Input, message, Modal} from "antd";
import {Button, Card, Checkbox} from "antd/es";
import {$stage} from "../../index";
import API from "../../api/api";

export class VRepoNew extends React.Component {
    isPrivate = false;

    render() {
        return (
            <div className="VFlex VFlex-Center">

                <Card style={{width: '400px', marginTop: "32px"}} title="Create new repository">
                    <Input placeholder="Name" ref="nr-name" onChange={(e) => {
                        this.refs["nr-path"].setState({value: e.target.value.trim().replace(/ /g, "-")});
                    }}/>
                    <Input placeholder="Path" style={{marginTop: '16px', marginBottom: '16px'}} ref="nr-path"/>
                    <Input placeholder="Description" ref="nr-desk" style={{marginBottom: '16px'}}/>
                    <div className="VFlex-Row">
                        <Checkbox ref="nr-private" style={{marginRight: '16px'}} onChange={() => {
                            this.isPrivate = !this.isPrivate;
                        }}>Private repository</Checkbox>
                        <Button onClick={() => {
                            const n = this.refs["nr-name"].state.value;
                            const p = this.refs["nr-path"].state.value;
                            const d = this.refs["nr-desk"].state.value;
                            const m = message.loading("Creating repository...", 0);
                            API.method("repository/create", {
                                token: $stage.app.token,
                                name: n,
                                path: p,
                                description: d,
                                confidential: this.isPrivate
                            }, (d) => {
                                m();
                                if (!!d.error) {
                                    Modal.error({
                                        title: "Create repository fail (" + d.error.code + ")",
                                        content: d.error.message
                                    });
                                    return;
                                }
                                API.updateMe((ok) => {
                                    if (!ok) {
                                        message.error("Invalid token!")
                                    } else {
                                        API.setPage("repo/" + d.path)
                                    }
                                })
                            })
                        }}>Submit</Button>
                    </div>
                </Card>
            </div>
        );
    }
}