import React from "react";
import {Avatar} from "antd";
import md5 from 'md5'

export default (props) => <Avatar className={props.className} style={props.style}
                                  src={"https://www.gravatar.com/avatar/" + md5(props.email) + "?s=" + (!!props.size ? props.size : (!!props.imgSize ? props.imgSize : "100"))}/>