import moment from "moment";
import {$event, $manager, $stage} from "../index";
import {message} from "antd";

export class VGitUtils {
    static getID(): string {
        return moment().valueOf() + this.makeID(15);
    }

    static makeID(length): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    static update(force) {
        if (force) {
            message.info("Calling an update...");

            $manager.user.getByToken($stage.app.token, loginResponse => {
                if (!loginResponse.ok) {
                    $stage.app.token = undefined;
                } else {
                    $stage.app.account = loginResponse.asUser();
                    $manager.repositorySearch.getByIds(loginResponse.asUser().repositories, $stage.app.token, reposResponse => {
                        $stage.app.repos = reposResponse.asRepoArray();
                    });
                }
            })
        } else {

        }
    }
}

export function runUtils() {
    $event.on("update_info", (f) => {
        VGitUtils.update(f === undefined ? false : f)
    });
}